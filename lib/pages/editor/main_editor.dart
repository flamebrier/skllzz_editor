import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:skllzz_editor/gena.dart';
import 'package:skllzz_editor/generated/models/models.pb.dart' as gena;
import 'package:skllzz_editor/providers/cache.dart';
import 'package:skllzz_editor/skllzz_app_icons.dart';
import 'package:widget_arrows/widget_arrows.dart';
import 'package:file_saver/file_saver.dart';

import '../../theming.dart';
import 'blocks.dart';
import 'draggable_block.dart';

gena.Script globalScript = gena.Script(quiz: gena.Quiz(actions: []));

sortByArrows() {
  final List<gena.Action> sortedActions = [];
  String newTarget = "";
  sortedActions.add(globalScript.quiz.actions.firstWhere((e) {
    final res = arrows.keys.contains(e.id) && !arrows.values.contains(e.id);
    if (res) {
      newTarget = arrows[e.id] ?? "";
    }
    return res;
  }));
  while (newTarget.isNotEmpty) {
    sortedActions.add(globalScript.quiz.actions.firstWhere((e) {
      final res = (e.id == newTarget);
      if (res) {
        newTarget = arrows[e.id] ?? "";
      }
      return res;
    }));
  }
  for (final one in sortedActions) {
    globalScript.quiz.actions.remove(one);
  }
  sortedActions.addAll(globalScript.quiz.actions);
  globalScript.quiz.actions.clear();
  globalScript.quiz.actions.addAll(sortedActions);
}

class MainEditor extends StatefulWidget {
  const MainEditor({Key? key}) : super(key: key);

  @override
  _MainEditorState createState() => _MainEditorState();
}

class _MainEditorState extends State<MainEditor> {
  final TextEditingController scriptNameController =
      TextEditingController(text: "SKLLZZ SCRIPT");

  @override
  void initState() {
    super.initState();
  }

  Offset offsetSide = Offset(-100, -50);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Styles.backgroundColor,
          actions: [
            Center(
              child: TextButton.icon(
                  onPressed: () async {
                    FilePickerResult? result = await FilePicker.platform
                        .pickFiles(
                            type: FileType.custom,
                            allowedExtensions: ["szd"],
                            allowMultiple: false);

                    if ((result != null) && result.files.isNotEmpty) {
                      globalScript = gena.Script.fromBuffer(
                          result.files.first.bytes?.toList() ?? []);
                    } else {
                      // User canceled the picker
                    }

                    scriptNameController.text = (result?.names.first ?? "skllzz script").split(".").first;

                    arrows.clear();
                    for (int i = 0;
                        i < globalScript.quiz.actions.length - 1;
                        i++) {
                      arrows.addAll({
                        globalScript.quiz.actions[i].id:
                            globalScript.quiz.actions[i + 1].id
                      });
                    }
                    if (mounted) setState(() {});
                  },
                  style: TextButton.styleFrom(
                      minimumSize: Size(150, 40),
                      primary: Styles.actionColor,
                      backgroundColor: Styles.actionColor.withOpacity(0.1)),
                  icon: Icon(Icons.upload_rounded, color: Styles.brandColor),
                  label: Text("Открыть черновик")),
            ),
            Container(width: 25),
            Center(
              child: TextButton.icon(
                  onPressed: () async {
                    sortByArrows();

                    final script =
                        generateSkllzzFromProto(globalScript.writeToBuffer());

                    MimeType type = MimeType.TEXT;
                    String? val = await FileSaver.instance.saveFile(
                      scriptNameController.text.isEmpty
                          ? "skllzz_script"
                          : scriptNameController.text,
                      script,
                      "skllzz",
                      mimeType: type,
                    );
                  },
                  style: TextButton.styleFrom(
                      minimumSize: Size(150, 40),
                      primary: Styles.actionColor,
                      backgroundColor: Styles.actionColor.withOpacity(0.1)),
                  icon: Icon(Icons.download_rounded,
                      color: Styles.blueAccentColor),
                  label: Text("Экспорт скрипта")),
            ),
            Container(width: 64),
          ],
          title: Container(
            width: 300,
            child: TextField(
              controller: scriptNameController,
              style: Theme.of(context).textTheme.headline5,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Styles.captionColor))),
            ),
          ),
        ),
        bottomNavigationBar: Card(
          elevation: 50,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              ElevatedButton(
                  onPressed: () async {
                    sortByArrows();

                    MimeType type = MimeType.TEXT;
                    String? val = await FileSaver.instance.saveFile(
                      scriptNameController.text.isEmpty
                          ? "game"
                          : scriptNameController.text,
                      globalScript.writeToBuffer(),
                      "szd",
                      mimeType: type,
                    );
                  },
                  child: Text("Сохранить")),
              Container(width: 15, height: 0),
              TextButton(
                  onPressed: () {
                    globalScript.clear();
                    setState(() {});
                  },
                  style: TextButton.styleFrom(
                    minimumSize: Size(150, 40),
                    primary: Styles.actionColor,
                  ),
                  child: Text("Отмена")),
            ]),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Styles.brandColor,
          child:
              Icon(SkllzzAppIcons.add, size: 48, color: Styles.backgroundCard),
          onPressed: () {
            setState(() {
              globalScript.quiz.actions.add(gena.Action(id: Cache.uuid.v4()));
            });
          },
        ),
        body: ArrowContainer(
          child: Stack(
              children: globalScript.quiz.actions.map((action) {
            final pb = PostBlock(action: action, elevation: 1);
            offsetSide = Offset(offsetSide.dx + 100, offsetSide.dy + 50);
            return DraggableBlock(
              initialPosition: offsetSide,
                action: action, builder: (context, elevation) => pb);
          }).toList()),
        ));
  }
}
