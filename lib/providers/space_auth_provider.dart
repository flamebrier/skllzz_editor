import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:grpc/grpc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/search.pb.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/space.pb.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/space.pbgrpc.dart';
import 'package:skllzz_editor/providers/provider.dart';
import 'package:skllzz_editor/widgets/wrappers/error.dart';

import 'info_provider.dart';

/// текущие пространства
final currentSpaceProvider =
    StateNotifierProvider((ref) => SpaceStateNotifier());

class SpaceStateNotifier extends StateNotifier<Space> {
  SpaceStateNotifier() : super(Space());

  Space get space {
    return state;
  }

  set space(Space space) {
    if ((space.id).isEmpty) {
      if ((state.id).isNotEmpty) {
        state = Space();
      }
    } else {
      state = space;
    }
  }

  exitSpace() {
    if ((state.id).isNotEmpty) {
      state = Space();
    }
  }
}

final cachedSpaceListProvider = StateProvider((ref) => []);

final spaceListProvider = StreamProvider<List<Space>>((ref) {
  SpaceAccessClient spaceClient = ref.watch(spaceAccessClientProvider);
  final errors = ref.watch(errorProvider);

  final res = StreamController<List<Space>>();
  final spacesList = List<Space>.empty(growable: true);

  ResponseStream<Space>? lastRequest;

  var active = true;

  ref.onDispose(() {
    active = false;
    lastRequest?.cancel();
    res.close();
  });

  ref.watch(authProvider).whenData((user) async {
    while (active && user!.uid.isNotEmpty) {
      try {
        lastRequest?.cancel();
        var firstResponse = true;
        lastRequest = spaceClient.monitor(QueryFilter());
        await for (final spacesBucket
            in lastRequest!.bufferTime(Duration(milliseconds: 100))) {
          if (spacesBucket.isNotEmpty || firstResponse) {
            if (firstResponse) {
              spacesList.clear();
            }
            firstResponse = false;
            for (final s in spacesBucket) {
              spacesList.retainWhere((element) => element.id != s.id);
              if (!s.deleted) {
                spacesList.add(s);
              }
/*              if (s.id == ref.read(currentSpaceProvider.state).id) {
                ref.read(currentSpaceProvider).space = s;
              }*/
            }
            spacesList.sort((a, b) => a.name.compareTo(b.name));
            res.add(spacesList);
            ref.read(cachedSpaceListProvider).state = spacesList;
          }
        }
      } catch (e) {
        if (active) {
          await Future.delayed(Duration(seconds: 1), () {
            errors.state = ExceptionCause(e, "Ошибка загрузки пространств");
          });
        }
      }
    }
  });

  return res.stream;
});

/// авторизация менеджера
final authProvider = StreamProvider<User?>((ref) {
  final streamController = StreamController<User?>();
  final _listener = FirebaseAuth.instance.authStateChanges().listen((user) {
    streamController.add(user);
  }, onError: (e, s) {
    ref.read(errorProvider).state = ExceptionCause(e, "Ошибка авторизации");
  });
  ref.onDispose(() {
    _listener.cancel();
  });

  ref.onDispose(() {
    streamController.close();
  });
  return streamController.stream;
});

final spaceAuthTokenProvider = StreamProvider<SpaceAuthorization?>((ref) {
  final spaceClient = ref.watch(spaceAccessClientProvider);
  final currentSpace = ref.watch(currentSpaceProvider.state);
  final streamController = StreamController<SpaceAuthorization?>();
  // bool isActive = true;
  ResponseStream<SpaceAuthorization>? lastRequest;
  ref.onDispose(() {
    lastRequest?.cancel();
    // isActive = false;
    streamController.close();
  });

  () async {
    if (currentSpace.id.isNotEmpty) {
      while (!streamController.isClosed) {
        lastRequest = spaceClient.auth(currentSpace);
        try {
          await for (final auth in lastRequest!) {
            if (!streamController.isClosed) {
              streamController.add(auth);
            }
          }
        } catch (e) {
          ref.read(errorProvider).state =
              ExceptionCause(e, "Ошибка входа в пространство");
        }
      }
    }
  }();

  return streamController.stream;
});
