import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:grpc/grpc.dart';
import 'package:grpc/grpc_web.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/script.pbgrpc.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/space.pbgrpc.dart';
import 'package:skllzz_editor/providers/space_auth_provider.dart';
import 'package:skllzz_editor/widgets/wrappers/error.dart';

import 'info_provider.dart';

GrpcWebClientChannel? overrideApiChannel;

/// канал соединения
final apiChannelProvider = Provider<GrpcWebClientChannel>((ref) =>
    overrideApiChannel ??
    GrpcWebClientChannel.xhr(Uri.parse("https://manager.api.skllzz.com")));

/// options
final spaceAuthOptionsProvider = Provider<CallOptions?>((ref) {
  final user = ref.watch(authProvider).data?.value;
  if (user == null) {
    return null;
  }
  final authenticate = (Map<String, String> metadata, String uri) async {
    //var refresh = forceRefreshToken;
    //forceRefreshToken = false;
    try {
      final jwt = (await user.getIdToken(false));
      if (jwt.isNotEmpty) {
        metadata['JWT'] = jwt;
      }
    } catch (e) {
      ref.read(errorProvider).state = ExceptionCause(e, "Ошибка авторизации");
    }
  };
  return CallOptions(providers: [authenticate]);
});

final accessAuthOptionsProvider = Provider<CallOptions>((ref) {
  final spaceToken = ref.watch(spaceAuthTokenProvider).data?.value;
  if (spaceToken == null || spaceToken.token.isEmpty) {
    return CallOptions();
  }
  final authenticate = (Map<String, String> metadata, String uri) async {
      metadata['JWT'] = spaceToken.token;
  };
  return CallOptions(providers: [authenticate]);
});

/// объекты доступа к бэку
final spaceAccessClientProvider = Provider<SpaceAccessClient>((ref) {
  final channel = ref.watch(apiChannelProvider);
  final options = ref.watch(spaceAuthOptionsProvider);
  return SpaceAccessClient(channel, options: options);
});

final scriptManagementClientProvider = Provider<ScriptManagementClient>((ref) {
  final channel = ref.watch(apiChannelProvider);
  final options = ref.watch(accessAuthOptionsProvider);
  return ScriptManagementClient(channel, options: options);
});