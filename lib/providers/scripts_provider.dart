import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:grpc/grpc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/script.pb.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/search.pb.dart';
import 'package:skllzz_editor/providers/provider.dart';
import 'package:skllzz_editor/providers/space_auth_provider.dart';
import 'package:skllzz_editor/widgets/wrappers/error.dart';

import 'cache.dart';
import 'info_provider.dart';

final scriptSearchProvider = StateProvider<String>((ref) => "");

final scriptListProvider = StreamProvider<List<Script>>((ref) {
  final client = ref.watch(scriptManagementClientProvider);
  final search = ref.watch(scriptSearchProvider);

  final _list = List<Script>.empty(growable: true);
  final _indexes = Map<String, int>();

  StreamController<List<Script>> _controller = StreamController<List<Script>>();

  ResponseStream<Script>? _lastRequest;
  final rqId = Cache.uuid.v4();
  String _requestUid = rqId;
  bool _isActive = true;

  ref.onDispose(() {
    _isActive = false;
    _controller.close();
  });

  ref.watch(spaceAuthTokenProvider).whenData((auth) async {
    while (_isActive && auth!.token.isNotEmpty && auth.roles.isNotEmpty) {
      try {
        _lastRequest?.cancel();
      } catch (_) {}
      _lastRequest =
          client.monitorScripts(QueryFilter()..search = search.state);
      try {
        if (_lastRequest != null) {
          _list.clear();
          await for (final values
              in _lastRequest!.bufferTime(Duration(milliseconds: 100))) {
            if (values.isNotEmpty) {
              if (_isActive) {
                for (final v in values) {
                  int index = _indexes[v.name] ?? -1;

                  if (index != -1) {
                    if (v.deleted) {
                      _list.removeAt(index);
                    } else {
                      if (v != _list[index]) {
                        _list[index] = v;
                      }
                    }
                  } else {
                    if (!v.deleted) {
                      _list.add(v);
                      _indexes.addAll({v.name: _list.length - 1});
                    }
                  }
                }
                _controller.add(_list);
              }
            }
          }
        }
        break;
      } catch (e) {
        if (_requestUid != rqId) {
          break;
        }
        await Future.delayed(Duration(seconds: 1), () {
          ref.read(errorProvider).state = ExceptionCause(e, "Ошибка загрузки");
        });
      } finally {
        _lastRequest?.cancel();
        _lastRequest = null;
      }
    }
  });

  return _controller.stream;
});
