import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:skllzz_editor/widgets/round_icon_button.dart';

import '../skllzz_app_icons.dart';
import '../theming.dart';
import 'adaptive/adaptive_widget.dart';

class SimpleSearchPanel extends StatefulWidget {
  final Function(String) onPressedAction;
  final Function()? onCancel;
  final InputDecoration? inputDecoration;
  final double initialButtonWidth;
  final double initialCancelButtonWidth;
  final double cancelButtonWidth;
  final String label;
  final EdgeInsets padding;
  final BorderRadiusGeometry borderRadius;

  const SimpleSearchPanel(
      {Key? key,
      required this.onPressedAction,
      this.inputDecoration,
      this.initialButtonWidth = 0,
      this.onCancel,
      this.initialCancelButtonWidth = 0,
      this.cancelButtonWidth = 50,
      this.padding = const EdgeInsets.all(8),
      this.label = "Искать",
      this.borderRadius = const BorderRadius.all(Radius.circular(50))})
      : super(key: key);

  @override
  _SimpleSearchPanelState createState() => _SimpleSearchPanelState();
}

class _SimpleSearchPanelState extends State<SimpleSearchPanel>
    with TickerProviderStateMixin {
  TextEditingController _searchController = TextEditingController();
  FocusNode _focusNode = FocusNode();

  late AnimationController _controller;
  late AnimationController _cancelController;
  late Animation<double> _scaleAnimation;
  late Animation<double> _cancelScaleAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: 300),
      lowerBound: 0,
      upperBound: 1,
      vsync: this,
    );
    _scaleAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInQuart,
    ));
    _cancelController = AnimationController(
      duration: Duration(milliseconds: 300),
      lowerBound: 0,
      upperBound: 1,
      vsync: this,
    );
    _cancelScaleAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _cancelController,
      curve: Curves.easeInQuart,
    ));
    _focusNode.addListener(() async {
      await activateSpoiler(_focusNode.hasFocus);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _cancelController.dispose();
    _focusNode.removeListener(() async {
      activateSpoiler(_focusNode.hasFocus);
    });
    _focusNode.dispose();
  }

  activateSpoiler(bool isOpened) async {
    if (mounted) {
      if (isOpened) {
        await _controller.forward();
      } else {
        await _controller.reverse();
      }
    }
  }

  activateCancelSpoiler(bool isOpened) async {
    if (mounted) {
      if (isOpened) {
        await _cancelController.forward();
      } else {
        await _cancelController.reverse();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Flex(direction: Axis.horizontal, children: [
      SizeTransition(
        axisAlignment: -1,
        axis: Axis.horizontal,
        sizeFactor: _cancelScaleAnimation,
        child: RoundIconButton(
          color: Styles.backgroundColor,
          highlightColor: Styles.actionColor.withOpacity(0.12),
          hoverColor: Styles.actionColor.withOpacity(0.12),
          splashColor: Styles.actionColor.withOpacity(0.12),
          icon: Icon(SkllzzAppIcons.close, color: Styles.actionColor, size: 32),
          onTap: () {
            activateCancelSpoiler(false);
            _searchController.clear();
            widget.onCancel!();
          },
        ),
      ),
      Expanded(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: TextField(
              focusNode: _focusNode,
              controller: _searchController,
              onSubmitted: _onSubmitted,
              decoration: widget.inputDecoration ??
                  InputDecoration(
                      labelText: "Поиск",
                      hintText:
                          "Ищите по телефону, никнейму или имени пользователя...",
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      border: InputBorder.none),
            )),
      ),
      AdaptiveWidget(
        tightChild: RoundIconButton(
          color: Styles.backgroundColor,
          highlightColor: Styles.actionColor.withOpacity(0.12),
          hoverColor: Styles.actionColor.withOpacity(0.12),
          splashColor: Styles.actionColor.withOpacity(0.12),
          icon: Icon(Icons.search_rounded, color: Styles.actionColor, size: 32),
          onTap: () {
            _onSubmitted((_searchController.text));
          },
        ),
        wideChild: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
                primary: Styles.backgroundColor,
                onPrimary: Styles.actionColor,
                minimumSize: Size(0, 40),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50))),
            onPressed: () {
              _onSubmitted((_searchController.text));
            },
            icon: Icon(Icons.search, size: 32),
            label: SizeTransition(
                axisAlignment: -1,
                axis: Axis.horizontal,
                sizeFactor: _scaleAnimation,
                child: Text(widget.label,
                    softWrap: false, textAlign: TextAlign.left))),
      )
    ]);
  }

  _onSubmitted(text) {
    activateCancelSpoiler(true);
    widget.onPressedAction(text);
  }
}
