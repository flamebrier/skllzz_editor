import 'package:flutter/material.dart';

class RoundIconButton extends StatelessWidget {
  final Color? color;
  final Color? highlightColor;
  final Color? hoverColor;
  final Color? splashColor;
  final GestureTapCallback? onTap;
  final GestureTapDownCallback? onTapDown;
  final Widget icon;
  final EdgeInsets? padding;

  RoundIconButton(
      {this.color,
      this.highlightColor,
      this.hoverColor,
      this.splashColor,
      this.onTap,
      this.onTapDown,
      required this.icon,
      this.padding});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: color ?? Theme.of(context).buttonColor,
      shape: CircleBorder(),
      clipBehavior: Clip.hardEdge,
      child: InkWell(
        highlightColor: highlightColor,
        hoverColor: hoverColor,
        splashColor: splashColor,
        onTap: onTap,
        onTapDown: onTapDown,
        child: Container(
          padding: padding ?? EdgeInsets.all(8),
          child: icon,
        ),
      ),
    );
  }
}
