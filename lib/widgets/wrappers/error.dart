import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:grpc/grpc.dart';
import 'package:skllzz_editor/providers/info_provider.dart';

import '../../theming.dart';

class ExceptionCause implements Exception {
  final dynamic cause;
  final String message;

  ExceptionCause(this.cause, this.message);
}

class InfoHelp {
  final String info;
  final Widget? help;

  InfoHelp(this.info, [this.help]);

  @override
  String toString() {
    return info;
  }
}

Future<dynamic> showErrorDialog(BuildContext context, dynamic e) {
  String? captionMsg = "Случилась непредвиденная ошибка!\n";
  var errorMsg = e?.toString();

  if (e is ExceptionCause) {
    captionMsg = e.message;
    errorMsg = e.cause.toString();
  }

  if (e is GrpcError) {
    if (e.code == StatusCode.cancelled) {
      return Future.value(null);
    }
    captionMsg = e.message;
    errorMsg = null;
  }
  if (e is NoSuchMethodError) {
    captionMsg = e.toString();
    errorMsg = null;
  }

  final children = <Widget>[
    Text(
      captionMsg!,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.bodyText2,
    ),
  ];

  if (errorMsg != null) {
    children.add(Container(height: 15));
    children.add(Text("Информация об ошибке:",
        style: Theme.of(context).textTheme.subtitle1,
        textAlign: TextAlign.center));
    children.add(
        SelectableText(errorMsg, style: Theme.of(context).textTheme.bodyText2));
  }

  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => ProviderScope(
              child: AlertDialog(
                  backgroundColor: Styles.backgroundDanger,
                  title: Text('Ошибка', textAlign: TextAlign.center),
                  content: Container(
                    constraints: BoxConstraints(maxWidth: 600),
                    child: SingleChildScrollView(
                      child: ListBody(children: children),
                    ),
                  ),
                  actions: <Widget>[
                Center(
                  child: TextButton(
                    child: Text(
                      'ОК',
                      style: Theme.of(context)
                          .textTheme
                          .button!
                          .copyWith(color: Styles.actionColor),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ])));
}

class ShowError extends StatefulWidget {
  final Widget child;

  const ShowError({Key? key, required this.child}) : super(key: key);

  @override
  _ShowErrorState createState() => _ShowErrorState();
}

class _ShowErrorState extends State<ShowError> {
  StreamSubscription? sub;
  String lastError = "";
  Duration snackBarDuration = Duration(seconds: 2);

  @override
  void initState() {
    super.initState();
    sub = context.read(errorProvider).stream.listen((err) {
      String key = err.toString();
      if (key.length > 30) {
        key = key.substring(0, 30);
      }
      if (err != null) {
        if (key != lastError) {
          lastError = key;
          String msg = err.toString();
          dynamic detailsErr = err;
          if (err is ExceptionCause) {
            key = err.message;
            if (key.length > 30) {
              key = key.substring(0, 30);
            }
            detailsErr = err.cause;

            if (detailsErr is Exception) {
              debugPrint(detailsErr.toString());
            }
            if (detailsErr is Error) {
              debugPrint(detailsErr.toString());
            }

            msg = "${err.message} $detailsErr";
          }

          if (detailsErr is GrpcError &&
              detailsErr.code == StatusCode.cancelled) {
            return;
          }
          if (detailsErr is GrpcError &&
              (detailsErr.code == StatusCode.internal)) {
            return;
          }

          final SnackBar snackBar = SnackBar(
            key: Key(key),
            duration: snackBarDuration,
            backgroundColor: Styles.backgroundDanger,
            content: InkWell(
                onTap: () {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                },
                child: Text(
                  msg,
                  style: Theme.of(context)
                      .textTheme
                      .caption!
                      .copyWith(color: Styles.actionColor),
                )),
            action: SnackBarAction(
              onPressed: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                showErrorDialog(context, detailsErr);
              },
              label: "Подробнее...",
              textColor: Styles.actionColor,
            ),
          );

          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          delay();
        }
      }
    });
  }

  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }

  delay() async {
    await Future.delayed(
        Duration(milliseconds: snackBarDuration.inMilliseconds + 1000), () {
      lastError = "";
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
