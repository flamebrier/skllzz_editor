import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/space.pbenum.dart';
import 'package:skllzz_editor/providers/info_provider.dart';
import 'package:skllzz_editor/providers/space_auth_provider.dart';

import 'error.dart';

class RoleRules {
  SpaceRole? role;
  bool Function(List<SpaceRole>)? roleChecker;

  RoleRules({this.role, this.roleChecker});

  RoleRules.role(this.role);

  RoleRules.check(this.roleChecker);

  bool check(List<SpaceRole> roles) {
    if (role != null) {
      if (roles.contains(role)) {
        return true;
      }
    } else if (roleChecker != null && roleChecker!(roles)) {
      return true;
    }
    return false;
  }
}

class RoleOnly extends ConsumerWidget {
  final RoleRules? rules;
  final Widget? child;
  final Widget? elseChild;

  RoleOnly({Key? key, this.rules, this.child, this.elseChild})
      : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final token = watch(spaceAuthTokenProvider);

    return token.when(
        data: (auth) {
          if (rules == null) {
            return child!;
          }
          if (rules!.check(auth!.roles)) {
            return child!;
          } else {
            return elseChild ?? Container();
          }
        },
        loading: () => Container(),
        error: (e, s) {
          context.read(errorProvider).state =
              ExceptionCause(e, "Ошибка определения ролей");
          return Container();
        });
  }
}
