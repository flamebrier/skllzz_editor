import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../theming.dart';

class CustomListTile extends StatelessWidget {
  final GestureTapCallback? onTap;
  final Widget? leading;
  final Widget? label;

  final Color? color;
  final Color? highlightColor;
  final Color? hoverColor;
  final Color? splashColor;
  final BorderRadius borderRadius;
  final EdgeInsets padding;

  final Widget? divider;

  const CustomListTile(
      {Key? key,
      this.onTap,
      this.leading,
      this.label,
      this.color,
      this.highlightColor,
      this.hoverColor,
      this.splashColor,
      this.borderRadius = const BorderRadius.all(Radius.circular(10)),
      this.divider,
      this.padding = const EdgeInsets.all(8)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Material(
            color: color ?? Styles.backgroundColor,
            child: InkWell(
              splashColor: splashColor,
              highlightColor: highlightColor,
              hoverColor: hoverColor,
              borderRadius: borderRadius,
              onTap: onTap,
              child: Container(
                padding: padding,
                child: Row(children: [
                  if (leading != null) leading!,
                  if (label != null)
                    Expanded(
                      child: label!,
                    ),
                ]),
              ),
            ),
          ),
          if (divider != null) divider!
        ]);
  }
}
