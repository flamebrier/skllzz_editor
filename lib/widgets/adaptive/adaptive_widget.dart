import 'package:flutter/material.dart';

import '../../theming.dart';

class AdaptiveWidget extends StatelessWidget {
  final Widget wideChild;
  final Widget tightChild;
  final double tightWidth;

  const AdaptiveWidget(
      {Key? key,
      required this.wideChild,
      required this.tightChild,
      this.tightWidth = 500})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width - Styles.currentNavWidth! <
        tightWidth) {
      return tightChild;
    }
    return wideChild;
  }
}
