import 'package:flutter/material.dart';

class AdaptiveInsets extends EdgeInsets {
  final bool isAdapt;

  AdaptiveInsets.all(this.isAdapt, double value)
      : super.fromLTRB(
            (isAdapt ? 0 : value), value, (isAdapt ? 0 : value), value);

  AdaptiveInsets.symmetric(this.isAdapt,
      {double vertical = 0, double horizontal = 0})
      : super.symmetric(
            vertical: vertical, horizontal: isAdapt ? 0 : horizontal);

  AdaptiveInsets.only(
    this.isAdapt, {
    double left = 0.0,
    double top = 0.0,
    double right = 0.0,
    double bottom = 0.0,
  }) : super.only(
          left: isAdapt ? 0 : left,
          top: top,
          right: isAdapt ? 0 : right,
          bottom: bottom,
        );

  AdaptiveInsets.fromLTRB(
      this.isAdapt, double left, double top, double right, double bottom)
      : super.fromLTRB(isAdapt ? 0 : left, top, isAdapt ? 0 : right, bottom);
}
