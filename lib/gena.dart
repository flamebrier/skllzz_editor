@JS()
library gena;

import 'dart:typed_data';

import 'package:js/js.dart';


@JS('gena.GenerateSkllzzFromProto')
external Uint8List generateSkllzzFromProto(Uint8List obj);

@JS('gena.BasicProtoStruct')
external Uint8List basicProtoStruct();

