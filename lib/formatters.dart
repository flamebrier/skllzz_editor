import 'package:intl/intl.dart';

String textDate(DateTime date, [DateFormat? format]) {
  final now = DateTime.now();
  final d1 = DateTime(date.year, date.month, date.day, 0, 0, 0, 0, 0);
  final d2 = DateTime(now.year, now.month, now.day, 0, 0, 0, 0, 0);
  final difference = d2.difference(d1).inDays;
  if (difference == 0) {
    return "Сегодня";
  }
  if (difference == 1) {
    return "Вчера";
  }
  if (format != null) {
    return format.format(date);
  }
  return DateFormat.yMMMMd().format(date);
}

DateTime dateFromSeconds(int seconds) {
  return DateTime.fromMillisecondsSinceEpoch(0)
      .add(Duration(seconds: (seconds)));
}

DateTime dateFromDays(int days) {
  return DateTime.fromMillisecondsSinceEpoch(0).add(Duration(days: (days)));
}

int secondsFromDate(DateTime date) {
  return Duration(milliseconds: date.millisecondsSinceEpoch).inSeconds;
}

int daysFromDate(DateTime date) {
  return Duration(milliseconds: date.millisecondsSinceEpoch).inDays;
}