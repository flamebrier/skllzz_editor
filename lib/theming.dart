import 'package:fixnum/fixnum.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';

@immutable
class Styles {
  static final globalInitialDate = Int32.MIN_VALUE.toInt();

  static final backgroundColor = Color.fromARGB(255, 33, 35, 34);
  static final backgroundDark = Color.fromRGBO(26, 26, 26, 0.6);
  static final backgroundCard = Color.fromARGB(255, 40, 40, 40);
  static final backgroundInfo = Color.fromARGB(255, 224, 224, 224);
  static final brandColor = Color.fromARGB(255, 252, 212, 0);
  static final greenAccentColor = Color.fromARGB(255, 6, 214, 160);
  static final brightGreenColor = Color(0xff47bf29);

  static final blueAccentColor = Color.fromARGB(255, 1, 116, 252);
  static final greyAccentColor = Color.fromARGB(255, 219, 255, 255);
  static final backgroundDanger = Color.fromARGB(255, 239, 62, 54);
  static final actionColor = Colors.white;
  static final reverseActionColor = Colors.black;
  static final answerTextColor = Colors.black;
  static final questionMessageColor = Colors.white.withOpacity(0.1);
  static final answerMessageColor = Colors.white.withOpacity(1.0);
  static final captionColor = Color.fromARGB(255, 145, 145, 145);

  static double currentNavWidth = 74;

  static final loadingAnimation = FlareActor("flare/beating_skllzz.flr",
      fit: BoxFit.fitWidth, animation: "beat", color: brandColor);

  static final TextTheme mainTextTheme = TextTheme(
    headline1: TextStyle(
      fontFamily: "Furore",
      fontSize: 70,
      letterSpacing: 3,
    ),
    headline2: TextStyle(
      fontFamily: "Furore",
      fontSize: 36,
      letterSpacing: -0.5,
    ),
    headline3: TextStyle(
      fontFamily: "GothaPro",
      fontSize: 28,
      fontWeight: FontWeight.w300,
    ),
    headline4:
    TextStyle(fontFamily: "Furore", fontSize: 28, letterSpacing: 0.3),
    headline5: TextStyle(
        fontFamily: "Furore", fontSize: 20, letterSpacing: 0.3, height: 1),
    headline6: TextStyle(
        fontFamily: "Furore", fontSize: 18, letterSpacing: 0.5, height: 1),
    subtitle1: TextStyle(
        fontFamily: "GothaPro",
        fontSize: 18,
        fontWeight: FontWeight.bold,
        letterSpacing: 0,
        height: 1.2),
    subtitle2: TextStyle(
        fontFamily: "GothaPro",
        fontSize: 15,
        fontWeight: FontWeight.bold,
        letterSpacing: 0,
        height: 1.2),
    bodyText1: TextStyle(fontFamily: "GothaPro", fontSize: 15, height: 1.2),
    bodyText2: TextStyle(fontFamily: "GothaPro", fontSize: 15, height: 1.2),
    button: TextStyle(fontFamily: "Furore", fontSize: 14, letterSpacing: 0.3),
    caption: TextStyle(
        fontFamily: "GothaPro",
        fontSize: 13,
        letterSpacing: 0,
        color: Styles.captionColor,
        height: 1.2),
    overline: TextStyle(
        fontFamily: "GothaPro",
        fontSize: 13,
        fontWeight: FontWeight.bold,
        color: Styles.captionColor,
        letterSpacing: 0,
        height: 1.2),
  );

  static final ThemeData mainTheme = ThemeData(
      brightness: Brightness.dark,
      primarySwatch: Colors.amber,
      backgroundColor: Styles.backgroundColor,
      scaffoldBackgroundColor: Styles.backgroundColor,
      primaryColor: Styles.brandColor,
      accentColor: Styles.brandColor,
      primaryColorDark: Styles.brandColor,
      canvasColor: Styles.backgroundCard,
      dialogBackgroundColor: Styles.backgroundColor,
      cardColor: Styles.backgroundCard,
      toggleableActiveColor: Styles.brandColor,
      splashColor: Styles.actionColor.withOpacity(0.2),
      hoverColor: Styles.actionColor.withOpacity(0.2),
      highlightColor: Styles.actionColor.withOpacity(0.1),
      focusColor: Styles.actionColor.withOpacity(0.1),
      iconTheme: IconThemeData(
        color: Colors.white60,
      ),
      textTheme: mainTextTheme,

      /// работа с полями
      inputDecorationTheme: InputDecorationTheme(
        hintStyle: TextStyle(
            fontFamily: "GothaPro",
            fontSize: 18,
            fontWeight: FontWeight.w400,
            color: Styles.actionColor.withOpacity(0.7)),
        labelStyle: TextStyle(
            fontFamily: "GothaPro", fontSize: 15, fontWeight: FontWeight.w500, color: Styles.actionColor),
        suffixStyle: TextStyle(
          fontFamily: "GothaPro",
          fontSize: 18,
          fontWeight: FontWeight.w600,
          letterSpacing: 0,
        ),
      ),

      /// всплывающие сообщения
      snackBarTheme: SnackBarThemeData(
          backgroundColor: Styles.backgroundInfo,
          elevation: 0,
          behavior: SnackBarBehavior.floating,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5)))),
      tooltipTheme: TooltipThemeData(
        textStyle: TextStyle(
            fontFamily: "GothaPro",
            fontSize: 13,
            fontWeight: FontWeight.w600,
            letterSpacing: 0,
            color: Styles.backgroundColor),
        decoration: BoxDecoration(
            color: backgroundInfo,
            borderRadius: BorderRadius.all(Radius.circular(5))),
      ),

      /// панели
      appBarTheme: AppBarTheme(
        textTheme: TextTheme(
          headline6: TextStyle(
            fontFamily: "Furore",
            fontSize: 21,
            fontWeight: FontWeight.w300,
            letterSpacing: 0.15,
            color: Colors.black,
          ),
        ),
      ),
      cardTheme: CardTheme(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))
      ),

      /// кнопки
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: brandColor,
            onPrimary: backgroundColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(21))),
            padding: EdgeInsets.symmetric(horizontal: 21, vertical: 16),
            textStyle: mainTextTheme.button,
            minimumSize: Size(200, 48),
          )),
      textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            primary: actionColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5))),
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            textStyle: mainTextTheme.button,
          )),
      outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(
            primary: backgroundInfo,
            textStyle: mainTextTheme.button,
            side: BorderSide(width: 2, color: backgroundInfo),
            padding: EdgeInsets.symmetric(horizontal: 21, vertical: 16),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(21))),
            minimumSize: Size(200, 48),
          )),

      /// диалоговые окна
      dialogTheme: DialogTheme(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(21))),
      timePickerTheme: TimePickerThemeData(
          dayPeriodTextStyle: TextStyle(fontFamily: "GothaPro", fontSize: 15)));

  static final ThemeData lightTheme = mainTheme.copyWith(
      brightness: Brightness.light,
      backgroundColor: backgroundInfo,
      scaffoldBackgroundColor: backgroundInfo,
      primaryColor: brandColor,
      accentColor: brandColor,
      primaryColorDark: brandColor,
      canvasColor: Styles.backgroundInfo,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: Colors.black,
        selectionColor: Styles.reverseActionColor.withOpacity(0.2),
        selectionHandleColor: Colors.amber.shade800,
      ),
      dialogBackgroundColor: Styles.backgroundInfo,
      cardColor: Colors.amber.withAlpha(150),
      toggleableActiveColor: Colors.amber.withAlpha(150),
      splashColor: Styles.reverseActionColor.withOpacity(0.2),
      hoverColor: Styles.reverseActionColor.withOpacity(0.2),
      highlightColor: Styles.reverseActionColor.withOpacity(0.1),
      iconTheme: IconThemeData(
        color: Colors.black38,
      ),
      textTheme: mainTheme.textTheme.merge(TextTheme(
          headline1: TextStyle(color: backgroundColor),
          headline2: TextStyle(color: backgroundColor),
          headline3: TextStyle(color: backgroundColor),
          headline4: TextStyle(color: backgroundColor),
          headline5: TextStyle(color: backgroundColor),
          headline6: TextStyle(color: backgroundColor),
          subtitle1: TextStyle(color: backgroundColor),
          subtitle2: TextStyle(color: backgroundColor),
          bodyText1: TextStyle(color: backgroundColor),
          bodyText2: TextStyle(color: backgroundColor),
          button: TextStyle(color: backgroundColor),
          caption: TextStyle(color: backgroundColor),
          overline: TextStyle(color: backgroundColor))),
      inputDecorationTheme: mainTheme.inputDecorationTheme.copyWith(
          labelStyle: mainTheme.inputDecorationTheme.labelStyle!
              .copyWith(color: Styles.backgroundColor)));
}

class LoadingErrorMessage extends StatelessWidget {
  final String message;

  const LoadingErrorMessage({Key? key, this.message = "Не удалось загрузить"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(children: [
          Text(message, style: Theme.of(context).textTheme.subtitle1),
          Text("Пробуем ещё...",
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(fontStyle: FontStyle.italic)),
          Container(width: 100, height: 100, child: Styles.loadingAnimation),
        ]));
  }
}
