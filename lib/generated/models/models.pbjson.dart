///
//  Generated code. Do not modify.
//  source: models.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use scriptDescriptor instead')
const Script$json = const {
  '1': 'Script',
  '2': const [
    const {'1': 'quiz', '3': 1, '4': 1, '5': 11, '6': '.com.skllzz.script.Quiz', '9': 0, '10': 'quiz'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `Script`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List scriptDescriptor = $convert.base64Decode('CgZTY3JpcHQSLQoEcXVpehgBIAEoCzIXLmNvbS5za2xsenouc2NyaXB0LlF1aXpIAFIEcXVpekIGCgR0eXBl');
@$core.Deprecated('Use quizDescriptor instead')
const Quiz$json = const {
  '1': 'Quiz',
  '2': const [
    const {'1': 'actions', '3': 1, '4': 3, '5': 11, '6': '.com.skllzz.script.Action', '10': 'actions'},
  ],
};

/// Descriptor for `Quiz`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List quizDescriptor = $convert.base64Decode('CgRRdWl6EjMKB2FjdGlvbnMYASADKAsyGS5jb20uc2tsbHp6LnNjcmlwdC5BY3Rpb25SB2FjdGlvbnM=');
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'message', '3': 2, '4': 1, '5': 11, '6': '.com.skllzz.script.Message', '9': 0, '10': 'message'},
    const {'1': 'question', '3': 3, '4': 1, '5': 11, '6': '.com.skllzz.script.Question', '9': 0, '10': 'question'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SDgoCaWQYASABKAlSAmlkEjYKB21lc3NhZ2UYAiABKAsyGi5jb20uc2tsbHp6LnNjcmlwdC5NZXNzYWdlSABSB21lc3NhZ2USOQoIcXVlc3Rpb24YAyABKAsyGy5jb20uc2tsbHp6LnNjcmlwdC5RdWVzdGlvbkgAUghxdWVzdGlvbkIGCgR0eXBl');
@$core.Deprecated('Use messageDescriptor instead')
const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'text', '3': 1, '4': 1, '5': 9, '10': 'text'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode('CgdNZXNzYWdlEhIKBHRleHQYASABKAlSBHRleHQ=');
@$core.Deprecated('Use questionDescriptor instead')
const Question$json = const {
  '1': 'Question',
  '2': const [
    const {'1': 'text', '3': 1, '4': 1, '5': 9, '10': 'text'},
    const {'1': 'answers', '3': 2, '4': 3, '5': 11, '6': '.com.skllzz.script.Answer', '10': 'answers'},
  ],
};

/// Descriptor for `Question`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questionDescriptor = $convert.base64Decode('CghRdWVzdGlvbhISCgR0ZXh0GAEgASgJUgR0ZXh0EjMKB2Fuc3dlcnMYAiADKAsyGS5jb20uc2tsbHp6LnNjcmlwdC5BbnN3ZXJSB2Fuc3dlcnM=');
@$core.Deprecated('Use answerDescriptor instead')
const Answer$json = const {
  '1': 'Answer',
  '2': const [
    const {'1': 'text', '3': 1, '4': 1, '5': 9, '10': 'text'},
    const {'1': 'award', '3': 2, '4': 1, '5': 2, '10': 'award'},
    const {'1': 'id', '3': 3, '4': 1, '5': 9, '10': 'id'},
  ],
};

/// Descriptor for `Answer`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List answerDescriptor = $convert.base64Decode('CgZBbnN3ZXISEgoEdGV4dBgBIAEoCVIEdGV4dBIUCgVhd2FyZBgCIAEoAlIFYXdhcmQSDgoCaWQYAyABKAlSAmlk');
