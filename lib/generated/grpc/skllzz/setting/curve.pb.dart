///
//  Generated code. Do not modify.
//  source: skllzz/setting/curve.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class MiningCurveParams extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MiningCurveParams', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hardnessBegin', $pb.PbFieldType.OD)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hardnessEnd', $pb.PbFieldType.OD)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'skllzzScale', $pb.PbFieldType.OD)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'polinomFactor', $pb.PbFieldType.O3)
    ..p<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', $pb.PbFieldType.PD)
    ..hasRequiredFields = false
  ;

  MiningCurveParams._() : super();
  factory MiningCurveParams({
    $core.double? hardnessBegin,
    $core.double? hardnessEnd,
    $core.double? skllzzScale,
    $core.int? polinomFactor,
    $core.Iterable<$core.double>? value,
  }) {
    final _result = create();
    if (hardnessBegin != null) {
      _result.hardnessBegin = hardnessBegin;
    }
    if (hardnessEnd != null) {
      _result.hardnessEnd = hardnessEnd;
    }
    if (skllzzScale != null) {
      _result.skllzzScale = skllzzScale;
    }
    if (polinomFactor != null) {
      _result.polinomFactor = polinomFactor;
    }
    if (value != null) {
      _result.value.addAll(value);
    }
    return _result;
  }
  factory MiningCurveParams.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MiningCurveParams.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MiningCurveParams clone() => MiningCurveParams()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MiningCurveParams copyWith(void Function(MiningCurveParams) updates) => super.copyWith((message) => updates(message as MiningCurveParams)) as MiningCurveParams; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MiningCurveParams create() => MiningCurveParams._();
  MiningCurveParams createEmptyInstance() => create();
  static $pb.PbList<MiningCurveParams> createRepeated() => $pb.PbList<MiningCurveParams>();
  @$core.pragma('dart2js:noInline')
  static MiningCurveParams getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MiningCurveParams>(create);
  static MiningCurveParams? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get hardnessBegin => $_getN(0);
  @$pb.TagNumber(1)
  set hardnessBegin($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasHardnessBegin() => $_has(0);
  @$pb.TagNumber(1)
  void clearHardnessBegin() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get hardnessEnd => $_getN(1);
  @$pb.TagNumber(2)
  set hardnessEnd($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasHardnessEnd() => $_has(1);
  @$pb.TagNumber(2)
  void clearHardnessEnd() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get skllzzScale => $_getN(2);
  @$pb.TagNumber(3)
  set skllzzScale($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSkllzzScale() => $_has(2);
  @$pb.TagNumber(3)
  void clearSkllzzScale() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get polinomFactor => $_getIZ(3);
  @$pb.TagNumber(4)
  set polinomFactor($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPolinomFactor() => $_has(3);
  @$pb.TagNumber(4)
  void clearPolinomFactor() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<$core.double> get value => $_getList(4);
}

