///
//  Generated code. Do not modify.
//  source: skllzz/setting/curve.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use miningCurveParamsDescriptor instead')
const MiningCurveParams$json = const {
  '1': 'MiningCurveParams',
  '2': const [
    const {'1': 'hardness_begin', '3': 1, '4': 1, '5': 1, '10': 'hardnessBegin'},
    const {'1': 'hardness_end', '3': 2, '4': 1, '5': 1, '10': 'hardnessEnd'},
    const {'1': 'skllzz_scale', '3': 3, '4': 1, '5': 1, '10': 'skllzzScale'},
    const {'1': 'polinom_factor', '3': 4, '4': 1, '5': 5, '10': 'polinomFactor'},
    const {'1': 'value', '3': 5, '4': 3, '5': 1, '10': 'value'},
  ],
};

/// Descriptor for `MiningCurveParams`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List miningCurveParamsDescriptor = $convert.base64Decode('ChFNaW5pbmdDdXJ2ZVBhcmFtcxIlCg5oYXJkbmVzc19iZWdpbhgBIAEoAVINaGFyZG5lc3NCZWdpbhIhCgxoYXJkbmVzc19lbmQYAiABKAFSC2hhcmRuZXNzRW5kEiEKDHNrbGx6el9zY2FsZRgDIAEoAVILc2tsbHp6U2NhbGUSJQoOcG9saW5vbV9mYWN0b3IYBCABKAVSDXBvbGlub21GYWN0b3ISFAoFdmFsdWUYBSADKAFSBXZhbHVl');
