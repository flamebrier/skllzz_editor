///
//  Generated code. Do not modify.
//  source: skllzz/common/geo.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use latLngDescriptor instead')
const LatLng$json = const {
  '1': 'LatLng',
  '2': const [
    const {'1': 'latitude', '3': 1, '4': 1, '5': 1, '10': 'latitude'},
    const {'1': 'longitude', '3': 2, '4': 1, '5': 1, '10': 'longitude'},
  ],
};

/// Descriptor for `LatLng`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List latLngDescriptor = $convert.base64Decode('CgZMYXRMbmcSGgoIbGF0aXR1ZGUYASABKAFSCGxhdGl0dWRlEhwKCWxvbmdpdHVkZRgCIAEoAVIJbG9uZ2l0dWRl');
@$core.Deprecated('Use bBoxDescriptor instead')
const BBox$json = const {
  '1': 'BBox',
  '2': const [
    const {'1': 'lo', '3': 1, '4': 1, '5': 11, '6': '.com.skllzz.api.LatLng', '10': 'lo'},
    const {'1': 'hi', '3': 2, '4': 1, '5': 11, '6': '.com.skllzz.api.LatLng', '10': 'hi'},
    const {'1': 'center', '3': 3, '4': 1, '5': 11, '6': '.com.skllzz.api.LatLng', '10': 'center'},
  ],
};

/// Descriptor for `BBox`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bBoxDescriptor = $convert.base64Decode('CgRCQm94EiYKAmxvGAEgASgLMhYuY29tLnNrbGx6ei5hcGkuTGF0TG5nUgJsbxImCgJoaRgCIAEoCzIWLmNvbS5za2xsenouYXBpLkxhdExuZ1ICaGkSLgoGY2VudGVyGAMgASgLMhYuY29tLnNrbGx6ei5hcGkuTGF0TG5nUgZjZW50ZXI=');
@$core.Deprecated('Use polygonDescriptor instead')
const Polygon$json = const {
  '1': 'Polygon',
  '2': const [
    const {'1': 'box', '3': 1, '4': 1, '5': 11, '6': '.com.skllzz.api.BBox', '10': 'box'},
    const {'1': 'vertex', '3': 2, '4': 3, '5': 11, '6': '.com.skllzz.api.LatLng', '10': 'vertex'},
  ],
};

/// Descriptor for `Polygon`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List polygonDescriptor = $convert.base64Decode('CgdQb2x5Z29uEiYKA2JveBgBIAEoCzIULmNvbS5za2xsenouYXBpLkJCb3hSA2JveBIuCgZ2ZXJ0ZXgYAiADKAsyFi5jb20uc2tsbHp6LmFwaS5MYXRMbmdSBnZlcnRleA==');
@$core.Deprecated('Use roiDescriptor instead')
const Roi$json = const {
  '1': 'Roi',
  '2': const [
    const {'1': 'area', '3': 2, '4': 3, '5': 11, '6': '.com.skllzz.api.Polygon', '10': 'area'},
  ],
};

/// Descriptor for `Roi`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List roiDescriptor = $convert.base64Decode('CgNSb2kSKwoEYXJlYRgCIAMoCzIXLmNvbS5za2xsenouYXBpLlBvbHlnb25SBGFyZWE=');
