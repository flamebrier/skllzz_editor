///
//  Generated code. Do not modify.
//  source: skllzz/common/common.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use posCategoryDescriptor instead')
const PosCategory$json = const {
  '1': 'PosCategory',
  '2': const [
    const {'1': 'other', '2': 0},
    const {'1': 'fitness', '2': 1},
    const {'1': 'food', '2': 3},
    const {'1': 'beauty', '2': 7},
    const {'1': 'clothes', '2': 5},
    const {'1': 'hardware', '2': 10},
    const {'1': 'grocery', '2': 4},
    const {'1': 'medicine', '2': 2},
    const {'1': 'entertainment', '2': 6},
    const {'1': 'travel', '2': 8},
    const {'1': 'pets', '2': 14},
    const {'1': 'education', '2': 9},
    const {'1': 'furniture', '2': 11},
    const {'1': 'fuel', '2': 12},
    const {'1': 'pharmacy', '2': 13},
  ],
};

/// Descriptor for `PosCategory`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List posCategoryDescriptor = $convert.base64Decode('CgtQb3NDYXRlZ29yeRIJCgVvdGhlchAAEgsKB2ZpdG5lc3MQARIICgRmb29kEAMSCgoGYmVhdXR5EAcSCwoHY2xvdGhlcxAFEgwKCGhhcmR3YXJlEAoSCwoHZ3JvY2VyeRAEEgwKCG1lZGljaW5lEAISEQoNZW50ZXJ0YWlubWVudBAGEgoKBnRyYXZlbBAIEggKBHBldHMQDhINCgllZHVjYXRpb24QCRINCglmdXJuaXR1cmUQCxIICgRmdWVsEAwSDAoIcGhhcm1hY3kQDQ==');
@$core.Deprecated('Use sexDescriptor instead')
const Sex$json = const {
  '1': 'Sex',
  '2': const [
    const {'1': 'undefined', '2': 0},
    const {'1': 'male', '2': 1},
    const {'1': 'female', '2': 2},
  ],
};

/// Descriptor for `Sex`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List sexDescriptor = $convert.base64Decode('CgNTZXgSDQoJdW5kZWZpbmVkEAASCAoEbWFsZRABEgoKBmZlbWFsZRAC');
@$core.Deprecated('Use levelDescriptor instead')
const Level$json = const {
  '1': 'Level',
  '2': const [
    const {'1': 'basic', '2': 0},
  ],
};

/// Descriptor for `Level`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List levelDescriptor = $convert.base64Decode('CgVMZXZlbBIJCgViYXNpYxAA');
@$core.Deprecated('Use linkDescriptor instead')
const Link$json = const {
  '1': 'Link',
  '2': const [
    const {'1': 'url', '3': 1, '4': 1, '5': 9, '10': 'url'},
  ],
};

/// Descriptor for `Link`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List linkDescriptor = $convert.base64Decode('CgRMaW5rEhAKA3VybBgBIAEoCVIDdXJs');
@$core.Deprecated('Use absoluteOfferDescriptor instead')
const AbsoluteOffer$json = const {
  '1': 'AbsoluteOffer',
  '2': const [
    const {'1': 'min', '3': 1, '4': 1, '5': 2, '10': 'min'},
    const {'1': 'max', '3': 2, '4': 1, '5': 2, '10': 'max'},
    const {'1': 'currency', '3': 3, '4': 1, '5': 9, '8': const {}, '10': 'currency'},
    const {'1': 'min_order_amount', '3': 4, '4': 1, '5': 2, '10': 'minOrderAmount'},
  ],
};

/// Descriptor for `AbsoluteOffer`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List absoluteOfferDescriptor = $convert.base64Decode('Cg1BYnNvbHV0ZU9mZmVyEhAKA21pbhgBIAEoAlIDbWluEhAKA21heBgCIAEoAlIDbWF4EiAKCGN1cnJlbmN5GAMgASgJQgSItRgBUghjdXJyZW5jeRIoChBtaW5fb3JkZXJfYW1vdW50GAQgASgCUg5taW5PcmRlckFtb3VudA==');
@$core.Deprecated('Use relativeOfferDescriptor instead')
const RelativeOffer$json = const {
  '1': 'RelativeOffer',
  '2': const [
    const {'1': 'min', '3': 1, '4': 1, '5': 2, '10': 'min'},
    const {'1': 'max', '3': 2, '4': 1, '5': 2, '10': 'max'},
    const {'1': 'currency', '3': 3, '4': 1, '5': 9, '8': const {}, '10': 'currency'},
    const {'1': 'min_order_amount', '3': 4, '4': 1, '5': 2, '10': 'minOrderAmount'},
  ],
};

/// Descriptor for `RelativeOffer`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List relativeOfferDescriptor = $convert.base64Decode('Cg1SZWxhdGl2ZU9mZmVyEhAKA21pbhgBIAEoAlIDbWluEhAKA21heBgCIAEoAlIDbWF4EiAKCGN1cnJlbmN5GAMgASgJQgSItRgBUghjdXJyZW5jeRIoChBtaW5fb3JkZXJfYW1vdW50GAQgASgCUg5taW5PcmRlckFtb3VudA==');
@$core.Deprecated('Use onlineDetailsDescriptor instead')
const OnlineDetails$json = const {
  '1': 'OnlineDetails',
  '2': const [
    const {'1': 'price', '3': 1, '4': 1, '5': 1, '10': 'price'},
    const {'1': 'offer', '3': 10, '4': 1, '5': 9, '8': const {}, '10': 'offer'},
    const {'1': 'coverage_id', '3': 20, '4': 1, '5': 9, '10': 'coverageId'},
    const {'1': 'coverage_name', '3': 21, '4': 1, '5': 9, '8': const {}, '10': 'coverageName'},
    const {'1': 'redirect_uri', '3': 30, '4': 1, '5': 9, '10': 'redirectUri'},
    const {'1': 'reusable', '3': 40, '4': 1, '5': 8, '10': 'reusable'},
    const {'1': 'hidden_code', '3': 50, '4': 1, '5': 8, '10': 'hiddenCode'},
  ],
};

/// Descriptor for `OnlineDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List onlineDetailsDescriptor = $convert.base64Decode('Cg1PbmxpbmVEZXRhaWxzEhQKBXByaWNlGAEgASgBUgVwcmljZRIaCgVvZmZlchgKIAEoCUIEiLUYAVIFb2ZmZXISHwoLY292ZXJhZ2VfaWQYFCABKAlSCmNvdmVyYWdlSWQSKQoNY292ZXJhZ2VfbmFtZRgVIAEoCUIEiLUYAVIMY292ZXJhZ2VOYW1lEiEKDHJlZGlyZWN0X3VyaRgeIAEoCVILcmVkaXJlY3RVcmkSGgoIcmV1c2FibGUYKCABKAhSCHJldXNhYmxlEh8KC2hpZGRlbl9jb2RlGDIgASgIUgpoaWRkZW5Db2Rl');
@$core.Deprecated('Use offlineDetailsDescriptor instead')
const OfflineDetails$json = const {
  '1': 'OfflineDetails',
  '2': const [
    const {'1': 'address', '3': 1, '4': 1, '5': 9, '8': const {}, '10': 'address'},
    const {'1': 'location', '3': 2, '4': 1, '5': 11, '6': '.com.skllzz.api.LatLng', '10': 'location'},
    const {'1': 'qr_link', '3': 3, '4': 1, '5': 9, '10': 'qrLink'},
    const {'1': 'pos_id', '3': 4, '4': 1, '5': 9, '10': 'posId'},
    const {'1': 'license_id', '3': 5, '4': 1, '5': 9, '10': 'licenseId'},
    const {'1': 'offer', '3': 10, '4': 1, '5': 9, '8': const {}, '10': 'offer'},
  ],
};

/// Descriptor for `OfflineDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List offlineDetailsDescriptor = $convert.base64Decode('Cg5PZmZsaW5lRGV0YWlscxIeCgdhZGRyZXNzGAEgASgJQgSItRgBUgdhZGRyZXNzEjIKCGxvY2F0aW9uGAIgASgLMhYuY29tLnNrbGx6ei5hcGkuTGF0TG5nUghsb2NhdGlvbhIXCgdxcl9saW5rGAMgASgJUgZxckxpbmsSFQoGcG9zX2lkGAQgASgJUgVwb3NJZBIdCgpsaWNlbnNlX2lkGAUgASgJUglsaWNlbnNlSWQSGgoFb2ZmZXIYCiABKAlCBIi1GAFSBW9mZmVy');
@$core.Deprecated('Use promoCodesDescriptor instead')
const PromoCodes$json = const {
  '1': 'PromoCodes',
  '2': const [
    const {'1': 'pos_id', '3': 1, '4': 1, '5': 9, '10': 'posId'},
    const {'1': 'codes', '3': 2, '4': 1, '5': 9, '10': 'codes'},
    const {'1': 'valid_until_epoch', '3': 3, '4': 1, '5': 3, '10': 'validUntilEpoch'},
    const {'1': 'valid_from_epoch', '3': 4, '4': 1, '5': 3, '10': 'validFromEpoch'},
  ],
};

/// Descriptor for `PromoCodes`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List promoCodesDescriptor = $convert.base64Decode('CgpQcm9tb0NvZGVzEhUKBnBvc19pZBgBIAEoCVIFcG9zSWQSFAoFY29kZXMYAiABKAlSBWNvZGVzEioKEXZhbGlkX3VudGlsX2Vwb2NoGAMgASgDUg92YWxpZFVudGlsRXBvY2gSKAoQdmFsaWRfZnJvbV9lcG9jaBgEIAEoA1IOdmFsaWRGcm9tRXBvY2g=');
@$core.Deprecated('Use promoCodeDescriptor instead')
const PromoCode$json = const {
  '1': 'PromoCode',
  '2': const [
    const {'1': 'code', '3': 1, '4': 1, '5': 9, '10': 'code'},
    const {'1': 'valid_until_epoch', '3': 3, '4': 1, '5': 3, '10': 'validUntilEpoch'},
    const {'1': 'valid_from_epoch', '3': 4, '4': 1, '5': 3, '10': 'validFromEpoch'},
  ],
};

/// Descriptor for `PromoCode`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List promoCodeDescriptor = $convert.base64Decode('CglQcm9tb0NvZGUSEgoEY29kZRgBIAEoCVIEY29kZRIqChF2YWxpZF91bnRpbF9lcG9jaBgDIAEoA1IPdmFsaWRVbnRpbEVwb2NoEigKEHZhbGlkX2Zyb21fZXBvY2gYBCABKANSDnZhbGlkRnJvbUVwb2No');
@$core.Deprecated('Use brandingDescriptor instead')
const Branding$json = const {
  '1': 'Branding',
  '2': const [
    const {'1': 'background_color', '3': 1, '4': 1, '5': 9, '10': 'backgroundColor'},
    const {'1': 'primary_color', '3': 2, '4': 1, '5': 9, '10': 'primaryColor'},
    const {'1': 'secondary_color', '3': 3, '4': 1, '5': 9, '10': 'secondaryColor'},
    const {'1': 'text_color', '3': 4, '4': 1, '5': 9, '10': 'textColor'},
  ],
};

/// Descriptor for `Branding`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List brandingDescriptor = $convert.base64Decode('CghCcmFuZGluZxIpChBiYWNrZ3JvdW5kX2NvbG9yGAEgASgJUg9iYWNrZ3JvdW5kQ29sb3ISIwoNcHJpbWFyeV9jb2xvchgCIAEoCVIMcHJpbWFyeUNvbG9yEicKD3NlY29uZGFyeV9jb2xvchgDIAEoCVIOc2Vjb25kYXJ5Q29sb3ISHQoKdGV4dF9jb2xvchgEIAEoCVIJdGV4dENvbG9y');
@$core.Deprecated('Use stepDetailsDescriptor instead')
const StepDetails$json = const {
  '1': 'StepDetails',
  '2': const [
    const {'1': 'steps', '3': 1, '4': 1, '5': 13, '10': 'steps'},
    const {'1': 'day', '3': 2, '4': 1, '5': 13, '10': 'day'},
    const {'1': 'meters', '3': 3, '4': 1, '5': 13, '10': 'meters'},
    const {'1': 'movement_factor', '3': 4, '4': 1, '5': 13, '10': 'movementFactor'},
  ],
};

/// Descriptor for `StepDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stepDetailsDescriptor = $convert.base64Decode('CgtTdGVwRGV0YWlscxIUCgVzdGVwcxgBIAEoDVIFc3RlcHMSEAoDZGF5GAIgASgNUgNkYXkSFgoGbWV0ZXJzGAMgASgNUgZtZXRlcnMSJwoPbW92ZW1lbnRfZmFjdG9yGAQgASgNUg5tb3ZlbWVudEZhY3Rvcg==');
@$core.Deprecated('Use hrDetailsDescriptor instead')
const HrDetails$json = const {
  '1': 'HrDetails',
  '2': const [
    const {'1': 'min_hr', '3': 1, '4': 1, '5': 13, '10': 'minHr'},
    const {'1': 'avg_hr', '3': 2, '4': 1, '5': 13, '10': 'avgHr'},
    const {'1': 'max_hr', '3': 3, '4': 1, '5': 13, '10': 'maxHr'},
    const {'1': 'min_hardness', '3': 4, '4': 1, '5': 1, '10': 'minHardness'},
    const {'1': 'avg_hardness', '3': 5, '4': 1, '5': 1, '10': 'avgHardness'},
    const {'1': 'max_hardness', '3': 6, '4': 1, '5': 1, '10': 'maxHardness'},
    const {'1': 'profile', '3': 7, '4': 1, '5': 11, '6': '.com.skllzz.api.Profile', '10': 'profile'},
    const {'1': 'device', '3': 8, '4': 3, '5': 9, '10': 'device'},
  ],
};

/// Descriptor for `HrDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List hrDetailsDescriptor = $convert.base64Decode('CglIckRldGFpbHMSFQoGbWluX2hyGAEgASgNUgVtaW5IchIVCgZhdmdfaHIYAiABKA1SBWF2Z0hyEhUKBm1heF9ochgDIAEoDVIFbWF4SHISIQoMbWluX2hhcmRuZXNzGAQgASgBUgttaW5IYXJkbmVzcxIhCgxhdmdfaGFyZG5lc3MYBSABKAFSC2F2Z0hhcmRuZXNzEiEKDG1heF9oYXJkbmVzcxgGIAEoAVILbWF4SGFyZG5lc3MSMQoHcHJvZmlsZRgHIAEoCzIXLmNvbS5za2xsenouYXBpLlByb2ZpbGVSB3Byb2ZpbGUSFgoGZGV2aWNlGAggAygJUgZkZXZpY2U=');
@$core.Deprecated('Use trainingSessionDescriptor instead')
const TrainingSession$json = const {
  '1': 'TrainingSession',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'sync_millis', '3': 2, '4': 1, '5': 3, '10': 'syncMillis'},
    const {'1': 'start_millis', '3': 3, '4': 1, '5': 3, '10': 'startMillis'},
    const {'1': 'stop_millis', '3': 4, '4': 1, '5': 3, '10': 'stopMillis'},
    const {'1': 'skllzz', '3': 6, '4': 1, '5': 1, '10': 'skllzz'},
    const {'1': 'deleted', '3': 7, '4': 1, '5': 8, '10': 'deleted'},
    const {'1': 'source_id', '3': 8, '4': 1, '5': 9, '10': 'sourceId'},
    const {'1': 'version', '3': 9, '4': 1, '5': 13, '10': 'version'},
    const {'1': 'steps', '3': 10, '4': 1, '5': 11, '6': '.com.skllzz.api.StepDetails', '9': 0, '10': 'steps'},
    const {'1': 'hr', '3': 11, '4': 1, '5': 11, '6': '.com.skllzz.api.HrDetails', '9': 0, '10': 'hr'},
    const {'1': 'timezone', '3': 12, '4': 1, '5': 9, '10': 'timezone'},
    const {'1': 'kkal', '3': 13, '4': 1, '5': 1, '10': 'kkal'},
    const {'1': 'profile_id', '3': 14, '4': 1, '5': 9, '10': 'profileId'},
  ],
  '8': const [
    const {'1': 'details'},
  ],
};

/// Descriptor for `TrainingSession`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List trainingSessionDescriptor = $convert.base64Decode('Cg9UcmFpbmluZ1Nlc3Npb24SDgoCaWQYASABKAlSAmlkEh8KC3N5bmNfbWlsbGlzGAIgASgDUgpzeW5jTWlsbGlzEiEKDHN0YXJ0X21pbGxpcxgDIAEoA1ILc3RhcnRNaWxsaXMSHwoLc3RvcF9taWxsaXMYBCABKANSCnN0b3BNaWxsaXMSFgoGc2tsbHp6GAYgASgBUgZza2xsenoSGAoHZGVsZXRlZBgHIAEoCFIHZGVsZXRlZBIbCglzb3VyY2VfaWQYCCABKAlSCHNvdXJjZUlkEhgKB3ZlcnNpb24YCSABKA1SB3ZlcnNpb24SMwoFc3RlcHMYCiABKAsyGy5jb20uc2tsbHp6LmFwaS5TdGVwRGV0YWlsc0gAUgVzdGVwcxIrCgJochgLIAEoCzIZLmNvbS5za2xsenouYXBpLkhyRGV0YWlsc0gAUgJochIaCgh0aW1lem9uZRgMIAEoCVIIdGltZXpvbmUSEgoEa2thbBgNIAEoAVIEa2thbBIdCgpwcm9maWxlX2lkGA4gASgJUglwcm9maWxlSWRCCQoHZGV0YWlscw==');
@$core.Deprecated('Use trainingDataDescriptor instead')
const TrainingData$json = const {
  '1': 'TrainingData',
  '2': const [
    const {'1': 'session_id', '3': 1, '4': 1, '5': 9, '10': 'sessionId'},
    const {'1': 'sync_millis', '3': 2, '4': 1, '5': 3, '10': 'syncMillis'},
    const {'1': 'stamp_millis', '3': 3, '4': 1, '5': 3, '10': 'stampMillis'},
    const {'1': 'device_id', '3': 4, '4': 1, '5': 9, '10': 'deviceId'},
    const {'1': 'device_name', '3': 5, '4': 1, '5': 9, '10': 'deviceName'},
    const {'1': 'hrAvg', '3': 12, '4': 1, '5': 1, '10': 'hrAvg'},
    const {'1': 'duration', '3': 13, '4': 1, '5': 5, '10': 'duration'},
    const {'1': 'skllzz', '3': 14, '4': 1, '5': 1, '10': 'skllzz'},
    const {'1': 'hardness', '3': 15, '4': 1, '5': 1, '10': 'hardness'},
    const {'1': 'version', '3': 16, '4': 1, '5': 13, '10': 'version'},
    const {'1': 'profile_id', '3': 17, '4': 1, '5': 9, '10': 'profileId'},
  ],
};

/// Descriptor for `TrainingData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List trainingDataDescriptor = $convert.base64Decode('CgxUcmFpbmluZ0RhdGESHQoKc2Vzc2lvbl9pZBgBIAEoCVIJc2Vzc2lvbklkEh8KC3N5bmNfbWlsbGlzGAIgASgDUgpzeW5jTWlsbGlzEiEKDHN0YW1wX21pbGxpcxgDIAEoA1ILc3RhbXBNaWxsaXMSGwoJZGV2aWNlX2lkGAQgASgJUghkZXZpY2VJZBIfCgtkZXZpY2VfbmFtZRgFIAEoCVIKZGV2aWNlTmFtZRIUCgVockF2ZxgMIAEoAVIFaHJBdmcSGgoIZHVyYXRpb24YDSABKAVSCGR1cmF0aW9uEhYKBnNrbGx6ehgOIAEoAVIGc2tsbHp6EhoKCGhhcmRuZXNzGA8gASgBUghoYXJkbmVzcxIYCgd2ZXJzaW9uGBAgASgNUgd2ZXJzaW9uEh0KCnByb2ZpbGVfaWQYESABKAlSCXByb2ZpbGVJZA==');
@$core.Deprecated('Use stepsDataDescriptor instead')
const StepsData$json = const {
  '1': 'StepsData',
  '2': const [
    const {'1': 'version', '3': 1, '4': 1, '5': 13, '10': 'version'},
    const {'1': 'sync_millis', '3': 2, '4': 1, '5': 3, '10': 'syncMillis'},
    const {'1': 'stamp_millis', '3': 3, '4': 1, '5': 3, '10': 'stampMillis'},
    const {'1': 'duration', '3': 4, '4': 1, '5': 5, '10': 'duration'},
    const {'1': 'steps', '3': 5, '4': 1, '5': 5, '10': 'steps'},
    const {'1': 'source_id', '3': 6, '4': 1, '5': 9, '10': 'sourceId'},
    const {'1': 'step_source', '3': 7, '4': 3, '5': 9, '10': 'stepSource'},
  ],
};

/// Descriptor for `StepsData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stepsDataDescriptor = $convert.base64Decode('CglTdGVwc0RhdGESGAoHdmVyc2lvbhgBIAEoDVIHdmVyc2lvbhIfCgtzeW5jX21pbGxpcxgCIAEoA1IKc3luY01pbGxpcxIhCgxzdGFtcF9taWxsaXMYAyABKANSC3N0YW1wTWlsbGlzEhoKCGR1cmF0aW9uGAQgASgFUghkdXJhdGlvbhIUCgVzdGVwcxgFIAEoBVIFc3RlcHMSGwoJc291cmNlX2lkGAYgASgJUghzb3VyY2VJZBIfCgtzdGVwX3NvdXJjZRgHIAMoCVIKc3RlcFNvdXJjZQ==');
@$core.Deprecated('Use profileDescriptor instead')
const Profile$json = const {
  '1': 'Profile',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '8': const {}, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '8': const {}, '10': 'name'},
    const {'1': 'nick_name', '3': 3, '4': 1, '5': 9, '8': const {}, '10': 'nickName'},
    const {'1': 'email', '3': 4, '4': 1, '5': 9, '8': const {}, '10': 'email'},
    const {'1': 'phone', '3': 5, '4': 1, '5': 9, '8': const {}, '10': 'phone'},
    const {'1': 'birth_date', '3': 6, '4': 1, '5': 5, '10': 'birthDate'},
    const {'1': 'hr_rest', '3': 7, '4': 1, '5': 5, '10': 'hrRest'},
    const {'1': 'hr_max', '3': 8, '4': 1, '5': 5, '10': 'hrMax'},
    const {'1': 'avatar_url', '3': 9, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'sex', '3': 11, '4': 1, '5': 14, '6': '.com.skllzz.api.Sex', '8': const {}, '10': 'sex'},
    const {'1': 'weight', '3': 12, '4': 1, '5': 2, '10': 'weight'},
    const {'1': 'join_stamp', '3': 13, '4': 1, '5': 3, '10': 'joinStamp'},
    const {'1': 'update_stamp', '3': 14, '4': 1, '5': 3, '10': 'updateStamp'},
    const {'1': 'tester', '3': 15, '4': 1, '5': 8, '10': 'tester'},
    const {'1': 'version', '3': 16, '4': 1, '5': 13, '10': 'version'},
    const {'1': 'achievements', '3': 17, '4': 1, '5': 11, '6': '.com.skllzz.api.Achievements', '10': 'achievements'},
    const {'1': 'timezone', '3': 18, '4': 1, '5': 9, '8': const {}, '10': 'timezone'},
    const {'1': 'garmin_access_token', '3': 19, '4': 1, '5': 9, '10': 'garminAccessToken'},
    const {'1': 'polar_access_token', '3': 20, '4': 1, '5': 9, '10': 'polarAccessToken'},
    const {'1': 'suunto_access_token', '3': 21, '4': 1, '5': 9, '10': 'suuntoAccessToken'},
    const {'1': 'fitbit_access_token', '3': 22, '4': 1, '5': 9, '10': 'fitbitAccessToken'},
    const {'1': 'invite_link', '3': 23, '4': 1, '5': 9, '10': 'inviteLink'},
    const {'1': 'lang', '3': 24, '4': 1, '5': 9, '8': const {}, '10': 'lang'},
    const {'1': 'subscriptions', '3': 30, '4': 3, '5': 11, '6': '.com.skllzz.api.Profile.SubscriptionsEntry', '8': const {}, '10': 'subscriptions'},
  ],
  '3': const [Profile_SubscriptionsEntry$json],
};

@$core.Deprecated('Use profileDescriptor instead')
const Profile_SubscriptionsEntry$json = const {
  '1': 'SubscriptionsEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 8, '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `Profile`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List profileDescriptor = $convert.base64Decode('CgdQcm9maWxlEhQKAmlkGAEgASgJQgSItRgBUgJpZBIYCgRuYW1lGAIgASgJQgSItRgBUgRuYW1lEiEKCW5pY2tfbmFtZRgDIAEoCUIEiLUYAVIIbmlja05hbWUSGgoFZW1haWwYBCABKAlCBIi1GAFSBWVtYWlsEhoKBXBob25lGAUgASgJQgSItRgBUgVwaG9uZRIdCgpiaXJ0aF9kYXRlGAYgASgFUgliaXJ0aERhdGUSFwoHaHJfcmVzdBgHIAEoBVIGaHJSZXN0EhUKBmhyX21heBgIIAEoBVIFaHJNYXgSHQoKYXZhdGFyX3VybBgJIAEoCVIJYXZhdGFyVXJsEisKA3NleBgLIAEoDjITLmNvbS5za2xsenouYXBpLlNleEIEiLUYAVIDc2V4EhYKBndlaWdodBgMIAEoAlIGd2VpZ2h0Eh0KCmpvaW5fc3RhbXAYDSABKANSCWpvaW5TdGFtcBIhCgx1cGRhdGVfc3RhbXAYDiABKANSC3VwZGF0ZVN0YW1wEhYKBnRlc3RlchgPIAEoCFIGdGVzdGVyEhgKB3ZlcnNpb24YECABKA1SB3ZlcnNpb24SQAoMYWNoaWV2ZW1lbnRzGBEgASgLMhwuY29tLnNrbGx6ei5hcGkuQWNoaWV2ZW1lbnRzUgxhY2hpZXZlbWVudHMSIAoIdGltZXpvbmUYEiABKAlCBIi1GAFSCHRpbWV6b25lEi4KE2dhcm1pbl9hY2Nlc3NfdG9rZW4YEyABKAlSEWdhcm1pbkFjY2Vzc1Rva2VuEiwKEnBvbGFyX2FjY2Vzc190b2tlbhgUIAEoCVIQcG9sYXJBY2Nlc3NUb2tlbhIuChNzdXVudG9fYWNjZXNzX3Rva2VuGBUgASgJUhFzdXVudG9BY2Nlc3NUb2tlbhIuChNmaXRiaXRfYWNjZXNzX3Rva2VuGBYgASgJUhFmaXRiaXRBY2Nlc3NUb2tlbhIfCgtpbnZpdGVfbGluaxgXIAEoCVIKaW52aXRlTGluaxIYCgRsYW5nGBggASgJQgSItRgBUgRsYW5nElYKDXN1YnNjcmlwdGlvbnMYHiADKAsyKi5jb20uc2tsbHp6LmFwaS5Qcm9maWxlLlN1YnNjcmlwdGlvbnNFbnRyeUIEiLUYAVINc3Vic2NyaXB0aW9ucxpAChJTdWJzY3JpcHRpb25zRW50cnkSEAoDa2V5GAEgASgJUgNrZXkSFAoFdmFsdWUYAiABKAhSBXZhbHVlOgI4AQ==');
@$core.Deprecated('Use achievementsDescriptor instead')
const Achievements$json = const {
  '1': 'Achievements',
  '2': const [
    const {'1': 'level', '3': 1, '4': 1, '5': 14, '6': '.com.skllzz.api.Level', '10': 'level'},
    const {'1': 'total_skllzz', '3': 2, '4': 1, '5': 5, '10': 'totalSkllzz'},
    const {'1': 'game_age_days', '3': 30, '4': 1, '5': 5, '10': 'gameAgeDays'},
    const {'1': 'iamok_skllzz_earned', '3': 4, '4': 1, '5': 5, '10': 'iamokSkllzzEarned'},
    const {'1': 'iamok_skllzz_required', '3': 5, '4': 1, '5': 5, '10': 'iamokSkllzzRequired'},
    const {'1': 'iamok_start', '3': 6, '4': 1, '5': 3, '10': 'iamokStart'},
    const {'1': 'iamok_duration', '3': 7, '4': 1, '5': 3, '10': 'iamokDuration'},
    const {'1': 'skllzz_day_limit', '3': 8, '4': 1, '5': 1, '10': 'skllzzDayLimit'},
    const {'1': 'iamok_uuid', '3': 9, '4': 1, '5': 9, '10': 'iamokUuid'},
  ],
};

/// Descriptor for `Achievements`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List achievementsDescriptor = $convert.base64Decode('CgxBY2hpZXZlbWVudHMSKwoFbGV2ZWwYASABKA4yFS5jb20uc2tsbHp6LmFwaS5MZXZlbFIFbGV2ZWwSIQoMdG90YWxfc2tsbHp6GAIgASgFUgt0b3RhbFNrbGx6ehIiCg1nYW1lX2FnZV9kYXlzGB4gASgFUgtnYW1lQWdlRGF5cxIuChNpYW1va19za2xsenpfZWFybmVkGAQgASgFUhFpYW1va1NrbGx6ekVhcm5lZBIyChVpYW1va19za2xsenpfcmVxdWlyZWQYBSABKAVSE2lhbW9rU2tsbHp6UmVxdWlyZWQSHwoLaWFtb2tfc3RhcnQYBiABKANSCmlhbW9rU3RhcnQSJQoOaWFtb2tfZHVyYXRpb24YByABKANSDWlhbW9rRHVyYXRpb24SKAoQc2tsbHp6X2RheV9saW1pdBgIIAEoAVIOc2tsbHp6RGF5TGltaXQSHQoKaWFtb2tfdXVpZBgJIAEoCVIJaWFtb2tVdWlk');
@$core.Deprecated('Use iamOkDescriptor instead')
const IamOk$json = const {
  '1': 'IamOk',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'total_skllzz', '3': 2, '4': 1, '5': 1, '10': 'totalSkllzz'},
    const {'1': 'game_age_days', '3': 30, '4': 1, '5': 5, '10': 'gameAgeDays'},
    const {'1': 'skllzz_earned', '3': 4, '4': 1, '5': 1, '10': 'skllzzEarned'},
    const {'1': 'skllzz_required', '3': 5, '4': 1, '5': 1, '10': 'skllzzRequired'},
    const {'1': 'start_seconds', '3': 6, '4': 1, '5': 3, '10': 'startSeconds'},
    const {'1': 'stop_seconds', '3': 7, '4': 1, '5': 3, '10': 'stopSeconds'},
  ],
};

/// Descriptor for `IamOk`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List iamOkDescriptor = $convert.base64Decode('CgVJYW1PaxIOCgJpZBgBIAEoCVICaWQSIQoMdG90YWxfc2tsbHp6GAIgASgBUgt0b3RhbFNrbGx6ehIiCg1nYW1lX2FnZV9kYXlzGB4gASgFUgtnYW1lQWdlRGF5cxIjCg1za2xsenpfZWFybmVkGAQgASgBUgxza2xsenpFYXJuZWQSJwoPc2tsbHp6X3JlcXVpcmVkGAUgASgBUg5za2xsenpSZXF1aXJlZBIjCg1zdGFydF9zZWNvbmRzGAYgASgDUgxzdGFydFNlY29uZHMSIQoMc3RvcF9zZWNvbmRzGAcgASgDUgtzdG9wU2Vjb25kcw==');
@$core.Deprecated('Use propertyDescriptor instead')
const Property$json = const {
  '1': 'Property',
  '2': const [
    const {'1': 'known', '3': 1, '4': 1, '5': 14, '6': '.com.skllzz.api.Property.Known', '9': 0, '10': 'known'},
    const {'1': 'custom', '3': 2, '4': 1, '5': 9, '9': 0, '10': 'custom'},
    const {'1': 'string', '3': 3, '4': 1, '5': 9, '9': 1, '10': 'string'},
    const {'1': 'int', '3': 4, '4': 1, '5': 3, '9': 1, '10': 'int'},
    const {'1': 'bool', '3': 5, '4': 1, '5': 8, '9': 1, '10': 'bool'},
    const {'1': 'float', '3': 6, '4': 1, '5': 2, '9': 1, '10': 'float'},
  ],
  '4': const [Property_Known$json],
  '8': const [
    const {'1': 'kind'},
    const {'1': 'value'},
  ],
};

@$core.Deprecated('Use propertyDescriptor instead')
const Property_Known$json = const {
  '1': 'Known',
  '2': const [
    const {'1': 'age', '2': 0},
    const {'1': 'skllzz', '2': 1},
  ],
};

/// Descriptor for `Property`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List propertyDescriptor = $convert.base64Decode('CghQcm9wZXJ0eRI2CgVrbm93bhgBIAEoDjIeLmNvbS5za2xsenouYXBpLlByb3BlcnR5Lktub3duSABSBWtub3duEhgKBmN1c3RvbRgCIAEoCUgAUgZjdXN0b20SGAoGc3RyaW5nGAMgASgJSAFSBnN0cmluZxISCgNpbnQYBCABKANIAVIDaW50EhQKBGJvb2wYBSABKAhIAVIEYm9vbBIWCgVmbG9hdBgGIAEoAkgBUgVmbG9hdCIcCgVLbm93bhIHCgNhZ2UQABIKCgZza2xsenoQAUIGCgRraW5kQgcKBXZhbHVl');
