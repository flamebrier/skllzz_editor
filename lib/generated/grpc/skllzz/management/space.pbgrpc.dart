///
//  Generated code. Do not modify.
//  source: skllzz/management/space.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'space.pb.dart' as $3;
import 'search.pb.dart' as $1;
export 'space.pb.dart';

class SpaceAccessClient extends $grpc.Client {
  static final _$auth = $grpc.ClientMethod<$3.Space, $3.SpaceAuthorization>(
      '/com.skllzz.api.SpaceAccess/Auth',
      ($3.Space value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.SpaceAuthorization.fromBuffer(value));
  static final _$enumerate = $grpc.ClientMethod<$1.QueryFilter, $3.Space>(
      '/com.skllzz.api.SpaceAccess/Enumerate',
      ($1.QueryFilter value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Space.fromBuffer(value));
  static final _$monitor = $grpc.ClientMethod<$1.QueryFilter, $3.Space>(
      '/com.skllzz.api.SpaceAccess/Monitor',
      ($1.QueryFilter value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Space.fromBuffer(value));
  static final _$join = $grpc.ClientMethod<$3.InviteTicket, $3.InviteTicket>(
      '/com.skllzz.api.SpaceAccess/Join',
      ($3.InviteTicket value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.InviteTicket.fromBuffer(value));

  SpaceAccessClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$3.SpaceAuthorization> auth($3.Space request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$auth, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseStream<$3.Space> enumerate($1.QueryFilter request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$enumerate, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseStream<$3.Space> monitor($1.QueryFilter request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$monitor, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$3.InviteTicket> join($3.InviteTicket request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$join, request, options: options);
  }
}

abstract class SpaceAccessServiceBase extends $grpc.Service {
  $core.String get $name => 'com.skllzz.api.SpaceAccess';

  SpaceAccessServiceBase() {
    $addMethod($grpc.ServiceMethod<$3.Space, $3.SpaceAuthorization>(
        'Auth',
        auth_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $3.Space.fromBuffer(value),
        ($3.SpaceAuthorization value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.QueryFilter, $3.Space>(
        'Enumerate',
        enumerate_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.QueryFilter.fromBuffer(value),
        ($3.Space value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.QueryFilter, $3.Space>(
        'Monitor',
        monitor_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.QueryFilter.fromBuffer(value),
        ($3.Space value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.InviteTicket, $3.InviteTicket>(
        'Join',
        join_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.InviteTicket.fromBuffer(value),
        ($3.InviteTicket value) => value.writeToBuffer()));
  }

  $async.Stream<$3.SpaceAuthorization> auth_Pre(
      $grpc.ServiceCall call, $async.Future<$3.Space> request) async* {
    yield* auth(call, await request);
  }

  $async.Stream<$3.Space> enumerate_Pre(
      $grpc.ServiceCall call, $async.Future<$1.QueryFilter> request) async* {
    yield* enumerate(call, await request);
  }

  $async.Stream<$3.Space> monitor_Pre(
      $grpc.ServiceCall call, $async.Future<$1.QueryFilter> request) async* {
    yield* monitor(call, await request);
  }

  $async.Future<$3.InviteTicket> join_Pre(
      $grpc.ServiceCall call, $async.Future<$3.InviteTicket> request) async {
    return join(call, await request);
  }

  $async.Stream<$3.SpaceAuthorization> auth(
      $grpc.ServiceCall call, $3.Space request);
  $async.Stream<$3.Space> enumerate(
      $grpc.ServiceCall call, $1.QueryFilter request);
  $async.Stream<$3.Space> monitor(
      $grpc.ServiceCall call, $1.QueryFilter request);
  $async.Future<$3.InviteTicket> join(
      $grpc.ServiceCall call, $3.InviteTicket request);
}

class SpaceManagementClient extends $grpc.Client {
  static final _$listSpace = $grpc.ClientMethod<$1.QueryFilter, $3.Space>(
      '/com.skllzz.api.SpaceManagement/ListSpace',
      ($1.QueryFilter value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Space.fromBuffer(value));
  static final _$updateSpace = $grpc.ClientMethod<$3.Space, $3.Space>(
      '/com.skllzz.api.SpaceManagement/UpdateSpace',
      ($3.Space value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Space.fromBuffer(value));
  static final _$listManager = $grpc.ClientMethod<$1.QueryFilter, $3.Manager>(
      '/com.skllzz.api.SpaceManagement/ListManager',
      ($1.QueryFilter value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Manager.fromBuffer(value));
  static final _$updateManager = $grpc.ClientMethod<$3.Manager, $3.Manager>(
      '/com.skllzz.api.SpaceManagement/UpdateManager',
      ($3.Manager value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Manager.fromBuffer(value));
  static final _$deleteManager = $grpc.ClientMethod<$3.Manager, $3.Manager>(
      '/com.skllzz.api.SpaceManagement/DeleteManager',
      ($3.Manager value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Manager.fromBuffer(value));
  static final _$invite = $grpc.ClientMethod<$3.Manager, $3.InviteTicket>(
      '/com.skllzz.api.SpaceManagement/Invite',
      ($3.Manager value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.InviteTicket.fromBuffer(value));

  SpaceManagementClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$3.Space> listSpace($1.QueryFilter request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$listSpace, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$3.Space> updateSpace($3.Space request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateSpace, request, options: options);
  }

  $grpc.ResponseStream<$3.Manager> listManager($1.QueryFilter request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$listManager, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$3.Manager> updateManager($3.Manager request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateManager, request, options: options);
  }

  $grpc.ResponseFuture<$3.Manager> deleteManager($3.Manager request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteManager, request, options: options);
  }

  $grpc.ResponseFuture<$3.InviteTicket> invite($3.Manager request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$invite, request, options: options);
  }
}

abstract class SpaceManagementServiceBase extends $grpc.Service {
  $core.String get $name => 'com.skllzz.api.SpaceManagement';

  SpaceManagementServiceBase() {
    $addMethod($grpc.ServiceMethod<$1.QueryFilter, $3.Space>(
        'ListSpace',
        listSpace_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.QueryFilter.fromBuffer(value),
        ($3.Space value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.Space, $3.Space>(
        'UpdateSpace',
        updateSpace_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.Space.fromBuffer(value),
        ($3.Space value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.QueryFilter, $3.Manager>(
        'ListManager',
        listManager_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.QueryFilter.fromBuffer(value),
        ($3.Manager value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.Manager, $3.Manager>(
        'UpdateManager',
        updateManager_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.Manager.fromBuffer(value),
        ($3.Manager value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.Manager, $3.Manager>(
        'DeleteManager',
        deleteManager_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.Manager.fromBuffer(value),
        ($3.Manager value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.Manager, $3.InviteTicket>(
        'Invite',
        invite_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.Manager.fromBuffer(value),
        ($3.InviteTicket value) => value.writeToBuffer()));
  }

  $async.Stream<$3.Space> listSpace_Pre(
      $grpc.ServiceCall call, $async.Future<$1.QueryFilter> request) async* {
    yield* listSpace(call, await request);
  }

  $async.Future<$3.Space> updateSpace_Pre(
      $grpc.ServiceCall call, $async.Future<$3.Space> request) async {
    return updateSpace(call, await request);
  }

  $async.Stream<$3.Manager> listManager_Pre(
      $grpc.ServiceCall call, $async.Future<$1.QueryFilter> request) async* {
    yield* listManager(call, await request);
  }

  $async.Future<$3.Manager> updateManager_Pre(
      $grpc.ServiceCall call, $async.Future<$3.Manager> request) async {
    return updateManager(call, await request);
  }

  $async.Future<$3.Manager> deleteManager_Pre(
      $grpc.ServiceCall call, $async.Future<$3.Manager> request) async {
    return deleteManager(call, await request);
  }

  $async.Future<$3.InviteTicket> invite_Pre(
      $grpc.ServiceCall call, $async.Future<$3.Manager> request) async {
    return invite(call, await request);
  }

  $async.Stream<$3.Space> listSpace(
      $grpc.ServiceCall call, $1.QueryFilter request);
  $async.Future<$3.Space> updateSpace($grpc.ServiceCall call, $3.Space request);
  $async.Stream<$3.Manager> listManager(
      $grpc.ServiceCall call, $1.QueryFilter request);
  $async.Future<$3.Manager> updateManager(
      $grpc.ServiceCall call, $3.Manager request);
  $async.Future<$3.Manager> deleteManager(
      $grpc.ServiceCall call, $3.Manager request);
  $async.Future<$3.InviteTicket> invite(
      $grpc.ServiceCall call, $3.Manager request);
}
