///
//  Generated code. Do not modify.
//  source: skllzz/management/script.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use dataBlockDescriptor instead')
const DataBlock$json = const {
  '1': 'DataBlock',
  '2': const [
    const {'1': 'data', '3': 2, '4': 1, '5': 12, '10': 'data'},
  ],
};

/// Descriptor for `DataBlock`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dataBlockDescriptor = $convert.base64Decode('CglEYXRhQmxvY2sSEgoEZGF0YRgCIAEoDFIEZGF0YQ==');
@$core.Deprecated('Use scriptDescriptor instead')
const Script$json = const {
  '1': 'Script',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '8': const {}, '10': 'name'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '8': const {}, '10': 'description'},
    const {'1': 'created_millis', '3': 3, '4': 1, '5': 3, '10': 'createdMillis'},
    const {'1': 'modified_millis', '3': 4, '4': 1, '5': 3, '10': 'modifiedMillis'},
    const {'1': 'deleted', '3': 999, '4': 1, '5': 8, '10': 'deleted'},
  ],
};

/// Descriptor for `Script`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List scriptDescriptor = $convert.base64Decode('CgZTY3JpcHQSGAoEbmFtZRgBIAEoCUIEiLUYAVIEbmFtZRImCgtkZXNjcmlwdGlvbhgCIAEoCUIEiLUYAVILZGVzY3JpcHRpb24SJQoOY3JlYXRlZF9taWxsaXMYAyABKANSDWNyZWF0ZWRNaWxsaXMSJwoPbW9kaWZpZWRfbWlsbGlzGAQgASgDUg5tb2RpZmllZE1pbGxpcxIZCgdkZWxldGVkGOcHIAEoCFIHZGVsZXRlZA==');
