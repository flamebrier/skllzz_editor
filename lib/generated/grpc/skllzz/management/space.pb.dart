///
//  Generated code. Do not modify.
//  source: skllzz/management/space.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'space.pbenum.dart';

export 'space.pbenum.dart';

class Space extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Space', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOB(9999, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deleted')
    ..hasRequiredFields = false
  ;

  Space._() : super();
  factory Space({
    $core.String? id,
    $core.String? name,
    $core.bool? deleted,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (deleted != null) {
      _result.deleted = deleted;
    }
    return _result;
  }
  factory Space.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Space.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Space clone() => Space()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Space copyWith(void Function(Space) updates) => super.copyWith((message) => updates(message as Space)) as Space; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Space create() => Space._();
  Space createEmptyInstance() => create();
  static $pb.PbList<Space> createRepeated() => $pb.PbList<Space>();
  @$core.pragma('dart2js:noInline')
  static Space getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Space>(create);
  static Space? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(9999)
  $core.bool get deleted => $_getBF(2);
  @$pb.TagNumber(9999)
  set deleted($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(9999)
  $core.bool hasDeleted() => $_has(2);
  @$pb.TagNumber(9999)
  void clearDeleted() => clearField(9999);
}

class Manager extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Manager', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..pc<SpaceRole>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'role', $pb.PbFieldType.PE, valueOf: SpaceRole.valueOf, enumValues: SpaceRole.values)
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'template')
    ..aOB(9999, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deleted')
    ..hasRequiredFields = false
  ;

  Manager._() : super();
  factory Manager({
    $core.String? id,
    $core.String? name,
    $core.Iterable<SpaceRole>? role,
    $core.bool? template,
    $core.bool? deleted,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (role != null) {
      _result.role.addAll(role);
    }
    if (template != null) {
      _result.template = template;
    }
    if (deleted != null) {
      _result.deleted = deleted;
    }
    return _result;
  }
  factory Manager.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Manager.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Manager clone() => Manager()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Manager copyWith(void Function(Manager) updates) => super.copyWith((message) => updates(message as Manager)) as Manager; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Manager create() => Manager._();
  Manager createEmptyInstance() => create();
  static $pb.PbList<Manager> createRepeated() => $pb.PbList<Manager>();
  @$core.pragma('dart2js:noInline')
  static Manager getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Manager>(create);
  static Manager? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<SpaceRole> get role => $_getList(2);

  @$pb.TagNumber(4)
  $core.bool get template => $_getBF(3);
  @$pb.TagNumber(4)
  set template($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTemplate() => $_has(3);
  @$pb.TagNumber(4)
  void clearTemplate() => clearField(4);

  @$pb.TagNumber(9999)
  $core.bool get deleted => $_getBF(4);
  @$pb.TagNumber(9999)
  set deleted($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(9999)
  $core.bool hasDeleted() => $_has(4);
  @$pb.TagNumber(9999)
  void clearDeleted() => clearField(9999);
}

class SpaceAuthorization extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SpaceAuthorization', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'relogin')
    ..pc<SpaceRole>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roles', $pb.PbFieldType.PE, valueOf: SpaceRole.valueOf, enumValues: SpaceRole.values)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'spaceId')
    ..hasRequiredFields = false
  ;

  SpaceAuthorization._() : super();
  factory SpaceAuthorization({
    $core.String? token,
    $core.bool? relogin,
    $core.Iterable<SpaceRole>? roles,
    $core.String? spaceId,
  }) {
    final _result = create();
    if (token != null) {
      _result.token = token;
    }
    if (relogin != null) {
      _result.relogin = relogin;
    }
    if (roles != null) {
      _result.roles.addAll(roles);
    }
    if (spaceId != null) {
      _result.spaceId = spaceId;
    }
    return _result;
  }
  factory SpaceAuthorization.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SpaceAuthorization.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SpaceAuthorization clone() => SpaceAuthorization()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SpaceAuthorization copyWith(void Function(SpaceAuthorization) updates) => super.copyWith((message) => updates(message as SpaceAuthorization)) as SpaceAuthorization; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SpaceAuthorization create() => SpaceAuthorization._();
  SpaceAuthorization createEmptyInstance() => create();
  static $pb.PbList<SpaceAuthorization> createRepeated() => $pb.PbList<SpaceAuthorization>();
  @$core.pragma('dart2js:noInline')
  static SpaceAuthorization getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SpaceAuthorization>(create);
  static SpaceAuthorization? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get relogin => $_getBF(1);
  @$pb.TagNumber(2)
  set relogin($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRelogin() => $_has(1);
  @$pb.TagNumber(2)
  void clearRelogin() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<SpaceRole> get roles => $_getList(2);

  @$pb.TagNumber(4)
  $core.String get spaceId => $_getSZ(3);
  @$pb.TagNumber(4)
  set spaceId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSpaceId() => $_has(3);
  @$pb.TagNumber(4)
  void clearSpaceId() => clearField(4);
}

class InviteTicket extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InviteTicket', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'spaceId')
    ..aOM<Manager>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'template', subBuilder: Manager.create)
    ..hasRequiredFields = false
  ;

  InviteTicket._() : super();
  factory InviteTicket({
    $core.String? id,
    $core.String? spaceId,
    Manager? template,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (spaceId != null) {
      _result.spaceId = spaceId;
    }
    if (template != null) {
      _result.template = template;
    }
    return _result;
  }
  factory InviteTicket.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InviteTicket.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InviteTicket clone() => InviteTicket()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InviteTicket copyWith(void Function(InviteTicket) updates) => super.copyWith((message) => updates(message as InviteTicket)) as InviteTicket; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InviteTicket create() => InviteTicket._();
  InviteTicket createEmptyInstance() => create();
  static $pb.PbList<InviteTicket> createRepeated() => $pb.PbList<InviteTicket>();
  @$core.pragma('dart2js:noInline')
  static InviteTicket getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InviteTicket>(create);
  static InviteTicket? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get spaceId => $_getSZ(1);
  @$pb.TagNumber(2)
  set spaceId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSpaceId() => $_has(1);
  @$pb.TagNumber(2)
  void clearSpaceId() => clearField(2);

  @$pb.TagNumber(3)
  Manager get template => $_getN(2);
  @$pb.TagNumber(3)
  set template(Manager v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasTemplate() => $_has(2);
  @$pb.TagNumber(3)
  void clearTemplate() => clearField(3);
  @$pb.TagNumber(3)
  Manager ensureTemplate() => $_ensure(2);
}

