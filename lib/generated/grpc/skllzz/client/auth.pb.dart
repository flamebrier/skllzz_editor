///
//  Generated code. Do not modify.
//  source: skllzz/client/auth.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class ProfileLinkRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ProfileLinkRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'botUrl')
    ..hasRequiredFields = false
  ;

  ProfileLinkRequest._() : super();
  factory ProfileLinkRequest({
    $core.String? id,
    $core.String? botUrl,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (botUrl != null) {
      _result.botUrl = botUrl;
    }
    return _result;
  }
  factory ProfileLinkRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ProfileLinkRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ProfileLinkRequest clone() => ProfileLinkRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ProfileLinkRequest copyWith(void Function(ProfileLinkRequest) updates) => super.copyWith((message) => updates(message as ProfileLinkRequest)) as ProfileLinkRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ProfileLinkRequest create() => ProfileLinkRequest._();
  ProfileLinkRequest createEmptyInstance() => create();
  static $pb.PbList<ProfileLinkRequest> createRepeated() => $pb.PbList<ProfileLinkRequest>();
  @$core.pragma('dart2js:noInline')
  static ProfileLinkRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ProfileLinkRequest>(create);
  static ProfileLinkRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get botUrl => $_getSZ(1);
  @$pb.TagNumber(2)
  set botUrl($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBotUrl() => $_has(1);
  @$pb.TagNumber(2)
  void clearBotUrl() => clearField(2);
}

class ProfileLinkResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ProfileLinkResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'url')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'profileId')
    ..hasRequiredFields = false
  ;

  ProfileLinkResponse._() : super();
  factory ProfileLinkResponse({
    $core.String? url,
    $core.String? profileId,
  }) {
    final _result = create();
    if (url != null) {
      _result.url = url;
    }
    if (profileId != null) {
      _result.profileId = profileId;
    }
    return _result;
  }
  factory ProfileLinkResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ProfileLinkResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ProfileLinkResponse clone() => ProfileLinkResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ProfileLinkResponse copyWith(void Function(ProfileLinkResponse) updates) => super.copyWith((message) => updates(message as ProfileLinkResponse)) as ProfileLinkResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ProfileLinkResponse create() => ProfileLinkResponse._();
  ProfileLinkResponse createEmptyInstance() => create();
  static $pb.PbList<ProfileLinkResponse> createRepeated() => $pb.PbList<ProfileLinkResponse>();
  @$core.pragma('dart2js:noInline')
  static ProfileLinkResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ProfileLinkResponse>(create);
  static ProfileLinkResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get url => $_getSZ(0);
  @$pb.TagNumber(1)
  set url($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUrl() => $_has(0);
  @$pb.TagNumber(1)
  void clearUrl() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get profileId => $_getSZ(1);
  @$pb.TagNumber(2)
  set profileId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasProfileId() => $_has(1);
  @$pb.TagNumber(2)
  void clearProfileId() => clearField(2);
}

class CustomAuthRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomAuthRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'uid')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'locale')
    ..hasRequiredFields = false
  ;

  CustomAuthRequest._() : super();
  factory CustomAuthRequest({
    $core.String? uid,
    $core.String? locale,
  }) {
    final _result = create();
    if (uid != null) {
      _result.uid = uid;
    }
    if (locale != null) {
      _result.locale = locale;
    }
    return _result;
  }
  factory CustomAuthRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomAuthRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomAuthRequest clone() => CustomAuthRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomAuthRequest copyWith(void Function(CustomAuthRequest) updates) => super.copyWith((message) => updates(message as CustomAuthRequest)) as CustomAuthRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomAuthRequest create() => CustomAuthRequest._();
  CustomAuthRequest createEmptyInstance() => create();
  static $pb.PbList<CustomAuthRequest> createRepeated() => $pb.PbList<CustomAuthRequest>();
  @$core.pragma('dart2js:noInline')
  static CustomAuthRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomAuthRequest>(create);
  static CustomAuthRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get uid => $_getSZ(0);
  @$pb.TagNumber(1)
  set uid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUid() => $_has(0);
  @$pb.TagNumber(1)
  void clearUid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get locale => $_getSZ(1);
  @$pb.TagNumber(2)
  set locale($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLocale() => $_has(1);
  @$pb.TagNumber(2)
  void clearLocale() => clearField(2);
}

enum CustomAuthState_Source {
  telegramUri, 
  token, 
  notSet
}

class CustomAuthState extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, CustomAuthState_Source> _CustomAuthState_SourceByTag = {
    1 : CustomAuthState_Source.telegramUri,
    2 : CustomAuthState_Source.token,
    0 : CustomAuthState_Source.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomAuthState', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'telegramUri')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..hasRequiredFields = false
  ;

  CustomAuthState._() : super();
  factory CustomAuthState({
    $core.String? telegramUri,
    $core.String? token,
  }) {
    final _result = create();
    if (telegramUri != null) {
      _result.telegramUri = telegramUri;
    }
    if (token != null) {
      _result.token = token;
    }
    return _result;
  }
  factory CustomAuthState.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomAuthState.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomAuthState clone() => CustomAuthState()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomAuthState copyWith(void Function(CustomAuthState) updates) => super.copyWith((message) => updates(message as CustomAuthState)) as CustomAuthState; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomAuthState create() => CustomAuthState._();
  CustomAuthState createEmptyInstance() => create();
  static $pb.PbList<CustomAuthState> createRepeated() => $pb.PbList<CustomAuthState>();
  @$core.pragma('dart2js:noInline')
  static CustomAuthState getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomAuthState>(create);
  static CustomAuthState? _defaultInstance;

  CustomAuthState_Source whichSource() => _CustomAuthState_SourceByTag[$_whichOneof(0)]!;
  void clearSource() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get telegramUri => $_getSZ(0);
  @$pb.TagNumber(1)
  set telegramUri($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTelegramUri() => $_has(0);
  @$pb.TagNumber(1)
  void clearTelegramUri() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get token => $_getSZ(1);
  @$pb.TagNumber(2)
  set token($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearToken() => clearField(2);
}

class JWT extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JWT', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phoneToken')
    ..hasRequiredFields = false
  ;

  JWT._() : super();
  factory JWT({
    $core.String? token,
    $core.String? phoneToken,
  }) {
    final _result = create();
    if (token != null) {
      _result.token = token;
    }
    if (phoneToken != null) {
      _result.phoneToken = phoneToken;
    }
    return _result;
  }
  factory JWT.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JWT.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JWT clone() => JWT()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JWT copyWith(void Function(JWT) updates) => super.copyWith((message) => updates(message as JWT)) as JWT; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JWT create() => JWT._();
  JWT createEmptyInstance() => create();
  static $pb.PbList<JWT> createRepeated() => $pb.PbList<JWT>();
  @$core.pragma('dart2js:noInline')
  static JWT getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JWT>(create);
  static JWT? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get phoneToken => $_getSZ(1);
  @$pb.TagNumber(2)
  set phoneToken($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPhoneToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearPhoneToken() => clearField(2);
}

class ImpersonateRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ImpersonateRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'com.skllzz.api'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'uid')
    ..hasRequiredFields = false
  ;

  ImpersonateRequest._() : super();
  factory ImpersonateRequest({
    $core.String? uid,
  }) {
    final _result = create();
    if (uid != null) {
      _result.uid = uid;
    }
    return _result;
  }
  factory ImpersonateRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ImpersonateRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ImpersonateRequest clone() => ImpersonateRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ImpersonateRequest copyWith(void Function(ImpersonateRequest) updates) => super.copyWith((message) => updates(message as ImpersonateRequest)) as ImpersonateRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ImpersonateRequest create() => ImpersonateRequest._();
  ImpersonateRequest createEmptyInstance() => create();
  static $pb.PbList<ImpersonateRequest> createRepeated() => $pb.PbList<ImpersonateRequest>();
  @$core.pragma('dart2js:noInline')
  static ImpersonateRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ImpersonateRequest>(create);
  static ImpersonateRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get uid => $_getSZ(0);
  @$pb.TagNumber(1)
  set uid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUid() => $_has(0);
  @$pb.TagNumber(1)
  void clearUid() => clearField(1);
}

