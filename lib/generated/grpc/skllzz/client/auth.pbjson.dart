///
//  Generated code. Do not modify.
//  source: skllzz/client/auth.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use profileLinkRequestDescriptor instead')
const ProfileLinkRequest$json = const {
  '1': 'ProfileLinkRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'bot_url', '3': 2, '4': 1, '5': 9, '10': 'botUrl'},
  ],
};

/// Descriptor for `ProfileLinkRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List profileLinkRequestDescriptor = $convert.base64Decode('ChJQcm9maWxlTGlua1JlcXVlc3QSDgoCaWQYASABKAlSAmlkEhcKB2JvdF91cmwYAiABKAlSBmJvdFVybA==');
@$core.Deprecated('Use profileLinkResponseDescriptor instead')
const ProfileLinkResponse$json = const {
  '1': 'ProfileLinkResponse',
  '2': const [
    const {'1': 'url', '3': 1, '4': 1, '5': 9, '10': 'url'},
    const {'1': 'profile_id', '3': 2, '4': 1, '5': 9, '10': 'profileId'},
  ],
};

/// Descriptor for `ProfileLinkResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List profileLinkResponseDescriptor = $convert.base64Decode('ChNQcm9maWxlTGlua1Jlc3BvbnNlEhAKA3VybBgBIAEoCVIDdXJsEh0KCnByb2ZpbGVfaWQYAiABKAlSCXByb2ZpbGVJZA==');
@$core.Deprecated('Use customAuthRequestDescriptor instead')
const CustomAuthRequest$json = const {
  '1': 'CustomAuthRequest',
  '2': const [
    const {'1': 'uid', '3': 1, '4': 1, '5': 9, '10': 'uid'},
    const {'1': 'locale', '3': 2, '4': 1, '5': 9, '10': 'locale'},
  ],
};

/// Descriptor for `CustomAuthRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customAuthRequestDescriptor = $convert.base64Decode('ChFDdXN0b21BdXRoUmVxdWVzdBIQCgN1aWQYASABKAlSA3VpZBIWCgZsb2NhbGUYAiABKAlSBmxvY2FsZQ==');
@$core.Deprecated('Use customAuthStateDescriptor instead')
const CustomAuthState$json = const {
  '1': 'CustomAuthState',
  '2': const [
    const {'1': 'telegram_uri', '3': 1, '4': 1, '5': 9, '9': 0, '10': 'telegramUri'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '9': 0, '10': 'token'},
  ],
  '8': const [
    const {'1': 'source'},
  ],
};

/// Descriptor for `CustomAuthState`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customAuthStateDescriptor = $convert.base64Decode('Cg9DdXN0b21BdXRoU3RhdGUSIwoMdGVsZWdyYW1fdXJpGAEgASgJSABSC3RlbGVncmFtVXJpEhYKBXRva2VuGAIgASgJSABSBXRva2VuQggKBnNvdXJjZQ==');
@$core.Deprecated('Use jWTDescriptor instead')
const JWT$json = const {
  '1': 'JWT',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'phone_token', '3': 2, '4': 1, '5': 9, '10': 'phoneToken'},
  ],
};

/// Descriptor for `JWT`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List jWTDescriptor = $convert.base64Decode('CgNKV1QSFAoFdG9rZW4YASABKAlSBXRva2VuEh8KC3Bob25lX3Rva2VuGAIgASgJUgpwaG9uZVRva2Vu');
@$core.Deprecated('Use impersonateRequestDescriptor instead')
const ImpersonateRequest$json = const {
  '1': 'ImpersonateRequest',
  '2': const [
    const {'1': 'uid', '3': 1, '4': 1, '5': 9, '10': 'uid'},
  ],
};

/// Descriptor for `ImpersonateRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List impersonateRequestDescriptor = $convert.base64Decode('ChJJbXBlcnNvbmF0ZVJlcXVlc3QSEAoDdWlkGAEgASgJUgN1aWQ=');
