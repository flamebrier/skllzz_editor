///
//  Generated code. Do not modify.
//  source: skllzz/client/auth.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'auth.pb.dart' as $0;
export 'auth.pb.dart';

class ExtAuthClient extends $grpc.Client {
  static final _$getCustomAppAuth =
      $grpc.ClientMethod<$0.CustomAuthRequest, $0.CustomAuthState>(
          '/com.skllzz.api.ExtAuth/GetCustomAppAuth',
          ($0.CustomAuthRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.CustomAuthState.fromBuffer(value));
  static final _$getProfileLink =
      $grpc.ClientMethod<$0.ProfileLinkRequest, $0.ProfileLinkResponse>(
          '/com.skllzz.api.ExtAuth/GetProfileLink',
          ($0.ProfileLinkRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ProfileLinkResponse.fromBuffer(value));
  static final _$cleanupProfileLink =
      $grpc.ClientMethod<$0.ProfileLinkRequest, $0.ProfileLinkResponse>(
          '/com.skllzz.api.ExtAuth/CleanupProfileLink',
          ($0.ProfileLinkRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ProfileLinkResponse.fromBuffer(value));
  static final _$mapAccount = $grpc.ClientMethod<$0.JWT, $0.JWT>(
      '/com.skllzz.api.ExtAuth/MapAccount',
      ($0.JWT value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.JWT.fromBuffer(value));
  static final _$impersonateAccount =
      $grpc.ClientMethod<$0.ImpersonateRequest, $0.JWT>(
          '/com.skllzz.api.ExtAuth/ImpersonateAccount',
          ($0.ImpersonateRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.JWT.fromBuffer(value));

  ExtAuthClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$0.CustomAuthState> getCustomAppAuth(
      $0.CustomAuthRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$getCustomAppAuth, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.ProfileLinkResponse> getProfileLink(
      $0.ProfileLinkRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getProfileLink, request, options: options);
  }

  $grpc.ResponseFuture<$0.ProfileLinkResponse> cleanupProfileLink(
      $0.ProfileLinkRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$cleanupProfileLink, request, options: options);
  }

  $grpc.ResponseFuture<$0.JWT> mapAccount($0.JWT request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$mapAccount, request, options: options);
  }

  $grpc.ResponseFuture<$0.JWT> impersonateAccount($0.ImpersonateRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$impersonateAccount, request, options: options);
  }
}

abstract class ExtAuthServiceBase extends $grpc.Service {
  $core.String get $name => 'com.skllzz.api.ExtAuth';

  ExtAuthServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CustomAuthRequest, $0.CustomAuthState>(
        'GetCustomAppAuth',
        getCustomAppAuth_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.CustomAuthRequest.fromBuffer(value),
        ($0.CustomAuthState value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ProfileLinkRequest, $0.ProfileLinkResponse>(
            'GetProfileLink',
            getProfileLink_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ProfileLinkRequest.fromBuffer(value),
            ($0.ProfileLinkResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ProfileLinkRequest, $0.ProfileLinkResponse>(
            'CleanupProfileLink',
            cleanupProfileLink_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ProfileLinkRequest.fromBuffer(value),
            ($0.ProfileLinkResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.JWT, $0.JWT>(
        'MapAccount',
        mapAccount_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.JWT.fromBuffer(value),
        ($0.JWT value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ImpersonateRequest, $0.JWT>(
        'ImpersonateAccount',
        impersonateAccount_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ImpersonateRequest.fromBuffer(value),
        ($0.JWT value) => value.writeToBuffer()));
  }

  $async.Stream<$0.CustomAuthState> getCustomAppAuth_Pre($grpc.ServiceCall call,
      $async.Future<$0.CustomAuthRequest> request) async* {
    yield* getCustomAppAuth(call, await request);
  }

  $async.Future<$0.ProfileLinkResponse> getProfileLink_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ProfileLinkRequest> request) async {
    return getProfileLink(call, await request);
  }

  $async.Future<$0.ProfileLinkResponse> cleanupProfileLink_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ProfileLinkRequest> request) async {
    return cleanupProfileLink(call, await request);
  }

  $async.Future<$0.JWT> mapAccount_Pre(
      $grpc.ServiceCall call, $async.Future<$0.JWT> request) async {
    return mapAccount(call, await request);
  }

  $async.Future<$0.JWT> impersonateAccount_Pre($grpc.ServiceCall call,
      $async.Future<$0.ImpersonateRequest> request) async {
    return impersonateAccount(call, await request);
  }

  $async.Stream<$0.CustomAuthState> getCustomAppAuth(
      $grpc.ServiceCall call, $0.CustomAuthRequest request);
  $async.Future<$0.ProfileLinkResponse> getProfileLink(
      $grpc.ServiceCall call, $0.ProfileLinkRequest request);
  $async.Future<$0.ProfileLinkResponse> cleanupProfileLink(
      $grpc.ServiceCall call, $0.ProfileLinkRequest request);
  $async.Future<$0.JWT> mapAccount($grpc.ServiceCall call, $0.JWT request);
  $async.Future<$0.JWT> impersonateAccount(
      $grpc.ServiceCall call, $0.ImpersonateRequest request);
}
