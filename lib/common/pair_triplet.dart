class Pair<T> {
  final T? a;
  final T? b;

  const Pair([this.a, this.b]);
}

class Triplet<T, S> extends Pair<T> {
  final S? c;

  const Triplet([a, b, this.c]) : super(a, b);
}

class SimpleTriplet<T> extends Pair<T> {
  final T c;

  const SimpleTriplet(a, b, this.c) : super(a, b);
}